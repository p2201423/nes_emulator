# Changelog

## [1.0a] - The CPU release
### Added

- Implemented every instructions from the 6502 instruction set
- Implemented the necessary stuctures for the CMOS6502 CPU
- Added CMake and building scripts
- Added Code of Conduct, README, CHANGELOG and LICENSE to the project
- Added gitlab pipeline to build and publish automatically the documentation
- Tested every instruction from the instruction set

