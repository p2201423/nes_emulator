# Project Global Configuration

cmake_minimum_required(VERSION 3.26)
project(PancakeNESEmu)

# Usefull when we want to configure the LSP Clangd Server for this project
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Find packages
find_package(PkgConfig REQUIRED)
find_package(Doxygen REQUIRED)

set(CURSES_NEED_NCURSES TRUE)
find_package(Curses REQUIRED)

# Compiler options

set(CMAKE_C_STANDARD 23)

set(CMAKE_C_FLAGS_DEBUG "-ggdb -O0")

set(CMAKE_C_FLAGS_RELEASE "-O3") 

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Wno-sequence-point")

# Set sources and includes
set(SOURCES 
    src/core/cpu.c
    src/core/mem.c
    src/rom_reader/reader.c
    src/UI/tui.c
)

set(INCLUDES
        include/
        ${CURSES_INCLUDE_DIR}
)

set(LIBRARIES
  ncursesw
  panel
)

add_executable(pancake_emu
        src/main.c
        ${SOURCES}
)

target_include_directories(pancake_emu PUBLIC ${INCLUDES})
target_link_libraries(pancake_emu PUBLIC ${LIBRARIES})

# Set output directories
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)

# Custom command for Doxygen
add_custom_target(
        doc ALL
        COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        COMMENT "Generating documentation with Doxygen"
        VERBATIM
)
