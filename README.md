[![C Project](https://img.shields.io/badge/C-00599C?style=for-the-badge&logo=c&logoColor=white)](https://www.iso.org/standard/74528.html)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)


# PancakeNESEmu
## Project Overview

This is a homebrew nes emulator written in C for Linux.

## Project Description

The goal of this project is to make a NES Emulator (more precisely, a 6502 CPU Emulator, a NES PPU Emulator and a APU). The graphics will be handled by a homemade graphics library.

The sound will be handled using PulseAudio.

## Project's Documentation

You can access the documentation [here](http://nes-emulator-p2201423-3e066329ec91ec6fdd321f43472e9f039218108d6.pages.univ-lyon1.fr/).

## Project Setup
### Prerequisites

On Debian based system:
```shell
sudo apt install graphviz doxygen gcc cmake make git build-essential -y 
```

On Fedora based system:
```shell
sudo dnf install cmake make git gcc 
```

On Arch based system:
```shell
sudo pacman -Sy gcc doxygen graphviz git cmake make
```

#### Install PulseAudio (without Pipewire)

On Debian based system:
```shell
sudo apt install pulseaudio libpulse-dev
```

On Fedora based system:
```shell
sudo dnf install pulseaudio pulseaudio-libs-devel
```

On Arch based system:
```shell
sudo pacman -S pulseaudio
```

#### Install PulseAudio (with Pipewire)

On Debian based system:
```shell
sudo apt install pipewire-pulse libpulse-dev
```

On Fedora based system:
```shell
sudo dnf install pipewire-pulseaudio pulseaudio pulseaudio-libs-devel
```

On Arch based system:
```
sudo pacman -S pipewire-pulse libpulse 
```

### Clone the project repo

Enter any folder you like and then run this command:
```shell
git clone https://forge.univ-lyon1.fr/p2201423/nes_emulator.git
cd nes_emulator
```

The command above will clone every file from our reposetory onto your disk and then place you in the correct working directory.

### Building our project

Just run the `build.sh` script and it will make a fresh and clean install of our project.


## Useful Tools

- The documentation is generated using [Doxygen](https://www.doxygen.nl/).
- The documentation overall theme is generated using [Doxygen Awesome](https://jothepro.github.io/doxygen-awesome-css/).
- The code is built using [CMake](https://cmake.org/).

## References

- [The Obelisk 6502 Guide](https://www.nesdev.org/obelisk-6502-guide/)
- [Emulator101 - 6502 Addressing Modes](http://www.emulator101.com/6502-addressing-modes.html)
- [NesDev Wiki](https://www.nesdev.org/wiki/Nesdev_Wiki)
- [NesDoug tutorial - 14. Intro to Sound](https://nesdoug.com/2015/12/02/14-intro-to-sound/)
- [PulseAudio C API Doc](https://www.freedesktop.org/software/pulseaudio/doxygen/index.html)

## Special Thanks

I would like to extend my deepest gratitude to the following community and people who have made a significant contribution to this project:

- [NesDev Community](https://discord.gg/JSG4kuF8EK): They have created so much ressources about the NES reverse engineering and been helpful when I had questions
- [NesHacker](https://www.youtube.com/c/NesHacker): They inspired me to start my coding adventure on the NES (game reverse engineering, ROM hacking, ...)
- [Tsoding](https://github.com/tsoding): They inspire me every day to try new things and to code dumb projects from scratch


