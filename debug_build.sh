#!/bin/bash

[ -e build/ ] && rm -r build/*
cmake -B build -DCMAKE_BUILD_TYPE=Debug && cmake --build build -j
