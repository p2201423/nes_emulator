#ifndef tui_h
#define tui_h

#include <locale.h>
#include <ncurses.h>

#include "core/cpu.h"

/**
 * @brief Procedure that init the ncurses screen
 * @param[in] local_format The locale text formatting
 * */
void init_nscreen(const char* local_format);

/**
 * @brief Procedure that displays the current state of the cpu in a ncurses window
 * @param[in] cpu The CPU to display
 * */
void printw_cpu_state(CPU* cpu);

/**
 * @brief Procedure that kill the current ncurses window
 * */
void end_nscreen();

#endif // !tui_h
