#ifndef apu_h
#define apu_h

#include "core/types.h"
#include "macros.h"

#include <pulse/pulseaudio.h>
#include <pulse/simple.h>

typedef struct {
    float frequency;
    float sample_rate;
    unsigned int volume;
    duty_cycle_t duty_cycle;
    wave_t wave_type;
} wave_settings_t;

/**
 * @brief Allocate a wave settings structure and return its pointers
 * @param[in] frequency The frequency of the wave
 * @param[in] sample_rate The sample rate of the wave
 * @param[in] volume The volume of the sound wave (also called amplitude)
 * @param[in] duty_cycle The duty_cycle of the wave (only used for Rectangle waveforms)
 * @param[in] wave_type The type of the wave
 * @return The pointer to the allocated settings
 * */
wave_settings_t* generate_wave_settings(float frequency, float sample_rate, unsigned int volume,
                                        duty_cycle_t duty_cycle, wave_t wave_type);

/**
 * @brief Free the wave_settings structure
 * @param[inout] wave_ss The pointer to the wave_settings
 * */
void free_wave_settings(wave_settings_t* wave_ss);

/**
 * @brief Generate a waveform inside of a buffer based on a frequency, rate, wave type and
 * duty_cycle
 * @param[inout] buf A pointer to the sound buffer
 * @param[in] size The size of the buffer
 * @param[in] wave_ss The sound settings of the wave
 * @return 0 if no error
 * */
errcode_t generate_wave(sbuffer_t* buf, size_t size, wave_settings_t* wave_ss);

/**
 * @brief Play a sound based on a sound buffer, a sound interface and a the buffer size
 * @param[in] buf A pointer to the sound buffer
 * @param[in] s A pointer to the sound interface
 * @param[in] size The size of the sound buffer
 * @return 0 if no error, 1 if pa_simple_write failed
 * */
errcode_t play_sound(sbuffer_t* buf, pa_simple* s, size_t size);

#endif // !apu_h
