#ifndef cpu_h
#define cpu_h

#include "core/mem.h"
#include "core/types.h"
#include "macros.h"

/**
 * @struct CPU
 * @brief This structure will be used to represent the state of the Central Processing Unit (CPU) of
 * our emulator. This Virtual CPU emulates a "MOS Technology 6502".
 *
 * Here's some informations about this CPU:
 * - It is a 8-bits register with a 16-bits addressing mode.
 * - There is 3 work registers: Accumulator, Register Index X, Register Index Y
 * - There is a Register that holds the processor intern flags. Here's its layout:
 *   - N: Negative. Set if the result of an operation is negative.
 *   - V: Overflow. Set if an arithmetic operation between two numbers cause and integer
 * overflow (For instance: 64+64=-128).
 *   - P: Unused on the NES, I'll use it as a "paused" state.
 *   - B: Break. Set if the BRK instruction is encountered or when an interrupt is raised.
 *   - D: Decimal. When set, the processor will obey the rule of BCD for add and sub.
 *   - I: Interrupt Disable. When set, the processor will not respond to any interrupt.
 *   - Z: Zero. Set if the result of the last operation is zero.
 *   - C: Carry. Set if the last operation caused an overflow from bit 7 or an underflow  from bit 0
 * */
typedef struct {
    address program_counter; ///< An address pointing to the next instruction to be executed
    data stack_pointer;      ///< A register that holds the 8 lower bytes of the address 0x11??.
    data cache;              ///< A 8 bit reserved space that I use for some operations
    data accumulator;        ///< A 8 bit register used to perform operations
    data register_x;         ///< A 8 bit register. Commonly used to hold offset and counters.
    data register_y;         ///< A 8 bit register. Commonly used to hold offset and counters.

    data flags; ///< NVPBDIZC

    unsigned char cycle_counter; ///< The cycle counter. The counter is decremented every step.

    memory mem; ///< A direct access to the Emulator Memory
} CPU;

/**
 * @brief Procedure that initialize the CPU to its boot/reset state
 * @param[inout] cpu The CPU to initialize
 * @param[in] mem The address to the memory array
 * */

void init_cpu(CPU* cpu, memory mem);

/**
 * @brief Procedure that resets the CPU by using the NES reset routine
 * @param[inout] processor The CPU to reset
 * */
void reset(CPU* processor);

/**
 * @brief Procedure that free the memory used by the CPU and the memory array
 * @param[inout] cpu The CPU to free and sets the pointer to NULL
 * */
void free_cpu(CPU* cpu);

/**
 * @brief Procedure that step a clock cycle
 * @param[inout] cpu The CPU to step
 * @return 0 if everything is fine, 0xff if there's a problem.
 * */
errcode_t step_cpu(CPU* cpu);

/**
 * @brief Procedure that get the next instruction written in the ROM
 * @param[in] cpu The CPU that contains the mem
 * */
unsigned char get_next_instruction(CPU* cpu);

/**
 * @brief Procedure that execute the current instruction in the ROM
 * @param[inout] cpu The CPU that execute the current instruction
 * */
errcode_t execute_instruction(CPU* cpu);

/**
 * @brief Procedure that displays the current state of the CPU
 * @param[in] processor The CPU to display
 * */
void print_cpu_state(CPU* processor);

/**
 * @brief Procedure that display the current state of the stack
 * @param[in] cpu The current cpu holding the stack
 * */
void print_stack_state(CPU* cpu);

/**
 * @brief Procedure that display the expected state of the stack
 * @param[in] stack An array representing the expected stack
 * @param[in] start The start of the stack
 * */
void print_stack_expected(memory stack, data start);

/**
 * @brief Procedure that display the expected state of the CPU
 * @param[in] acc The expected state of the accumulator
 * @param[in] x The expected state of the X register
 * @param[in] y The expected state of the Y register
 * @param[in] flags The expected flags
 * @param[in] pc The expected program counter
 * @param[in] sp The expected Stack pointer
 * */
void print_cpu_expected(data acc, data x, data y, data flags, address pc, data sp);

#define GET_C_FLAG(x) (x->flags & BIT_0_MASK)
#define GET_Z_FLAG(x) (x->flags & BIT_1_MASK)
#define GET_I_FLAG(x) (x->flags & BIT_2_MASK)
#define GET_D_FLAG(x) (x->flags & BIT_3_MASK)
#define GET_B_FLAG(x) (x->flags & BIT_4_MASK)
#define GET_P_FLAG(x) (x->flags & BIT_5_MASK)
#define GET_V_FLAG(x) (x->flags & BIT_6_MASK)
#define GET_N_FLAG(x) (x->flags & BIT_7_MASK)

#define SET_C_FLAG(x) (x->flags = x->flags | BIT_0_MASK)
#define SET_Z_FLAG(x) (x->flags = x->flags | BIT_1_MASK)
#define SET_I_FLAG(x) (x->flags = x->flags | BIT_2_MASK)
#define SET_D_FLAG(x) (x->flags = x->flags | BIT_3_MASK)
#define SET_B_FLAG(x) (x->flags = x->flags | BIT_4_MASK)
#define SET_P_FLAG(x) (x->flags = x->flags | BIT_5_MASK)
#define SET_V_FLAG(x) (x->flags = x->flags | BIT_6_MASK)
#define SET_N_FLAG(x) (x->flags = x->flags | BIT_7_MASK)

#define SET_ALL_FLAGS(x) (x->flags = x->flags | 0xFF)

#define CLEAR_C_FLAG(x) (x->flags = x->flags & NBIT_0_MASK)
#define CLEAR_Z_FLAG(x) (x->flags = x->flags & NBIT_1_MASK)
#define CLEAR_I_FLAG(x) (x->flags = x->flags & NBIT_2_MASK)
#define CLEAR_D_FLAG(x) (x->flags = x->flags & NBIT_3_MASK)
#define CLEAR_B_FLAG(x) (x->flags = x->flags & NBIT_4_MASK)
#define CLEAR_P_FLAG(x) (x->flags = x->flags & NBIT_5_MASK)
#define CLEAR_V_FLAG(x) (x->flags = x->flags & NBIT_6_MASK)
#define CLEAR_N_FLAG(x) (x->flags = x->flags & NBIT_7_MASK)

#define CLEAR_ALL_FLAGS(x) (x->flags = x->flags & 0x00)

/**
 * @brief Macro to access memory by indexed indirect addressing mode
 * @param[inout] cpu The CPU in which the memory field is located
 * @param[in] addr The base address (A ZERO PAGE ADDRESS ONLY)
 * @param[in] offset The offset (usually X)
 * */
#define indexed_indirect(cpu, addr, offset)                                                        \
    cpu->mem[(cpu->mem[(addr + offset) & 0xFF]) | (cpu->mem[(addr + offset + 1) & 0xFF] << 8)]

/**
 * @brief Macro to access memory by indirect indexed addressing mode
 * @param[inout] cpu The CPU in which the memory field is located
 * @param[in] addr The base address
 * @param[in] offset The offset (usually Y)
 * */
#define indirect_indexed(cpu, addr, offset)                                                        \
    cpu->mem[(cpu->mem[(addr) & 0xFF] | cpu->mem[(addr + 1) & 0xFF] << 8) + offset]

/**
 * @brief Macro to access memory by indirect addressing mode
 * @param[inout] cpu The CPU in which the memory field is located
 * @param[in] addr The base address
 * */
#define indirect(cpu, addr) cpu->mem[((cpu->mem[(addr)]) | (cpu->mem[(addr) + 1] << 8))]

/**
 * @brief Macro to recreate the current address pointed by pc + 1
 * @param[in] cpu The CPU that holds the pc
 * @return The reconstructed address
 * */
#define get_address(cpu)                                                                           \
    (cpu->mem[cpu->program_counter + 1] | (cpu->mem[cpu->program_counter + 2] << 8))

/**
 * @brief Macro to know if we cross a page when doing the instruction
 * @param[in] addr Base address
 * @param[in] offset Offset from base address
 * @return 0 if we stay on the current page, 1 if not
 * */
#define is_page_crossed(addr, offset) ((addr & 0x00ff) && !((addr + offset) & 0x00ff))

/**
 * @brief Procedure that flips the correct flags when loading a value in a register
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register in which the data has been loaded
 * */
#define LD_FLAGS_GENERIC(cpu, reg)                                                                 \
    ({                                                                                             \
        CLEAR_C_FLAG(cpu);                                                                         \
        if (cpu->reg == 0) {                                                                       \
            SET_Z_FLAG(cpu);                                                                       \
            CLEAR_N_FLAG(cpu);                                                                     \
        } else if (cpu->reg & BIT_7_MASK) {                                                        \
            SET_N_FLAG(cpu);                                                                       \
            CLEAR_Z_FLAG(cpu);                                                                     \
        } else {                                                                                   \
            CLEAR_N_FLAG(cpu);                                                                     \
            CLEAR_Z_FLAG(cpu);                                                                     \
        }                                                                                          \
    })

/**
 * @brief Procedure that loads an immediate (const value) in a register
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register in which we need to load the data
 * @param[in] d the data to load
 * */
#define LD_immediate_GENERIC(cpu, reg, d)                                                          \
    ({                                                                                             \
        CLEAR_C_FLAG(cpu);                                                                         \
        if ((d) == 0) {                                                                            \
            SET_Z_FLAG(cpu);                                                                       \
            CLEAR_N_FLAG(cpu);                                                                     \
        } else if ((d) & BIT_7_MASK) {                                                             \
            CLEAR_Z_FLAG(cpu);                                                                     \
            SET_N_FLAG(cpu);                                                                       \
        } else {                                                                                   \
            CLEAR_Z_FLAG(cpu);                                                                     \
            CLEAR_N_FLAG(cpu);                                                                     \
        }                                                                                          \
        cpu->reg = (d);                                                                            \
    })
/**
 * @brief Procedure that loads an immediate (const value) in the accumulator
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] d the data to load
 * */
#define LD_immediate_ACC(processor, d) (LD_immediate_GENERIC(processor, accumulator, d))

/**
 * @brief Procedure that loads an immediate (const value) in the X register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] d the data to load
 * */
#define LD_immediate_X(processor, d) (LD_immediate_GENERIC(processor, register_x, d))

/**
 * @brief Procedure that loads an immediate (const value) in the Y register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] d the data to load
 * */
#define LD_immediate_Y(processor, d) (LD_immediate_GENERIC(processor, register_y, d))

/**
 * @brief Procedure that loads a value from memory at address addr+offset to a register
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register in which we need to load the data
 * @param[in] addr The base address
 * @param[in] offset The offset from the address
 * */
#define LD_offset_GENERIC(cpu, reg, addr, offset)                                                  \
    LD_immediate_GENERIC(cpu, reg, cpu->mem[addr + offset])

/**
 * @brief Procedure that loads a value from memory at address addr+offset to the X register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The base address
 * @param[in] offset The offset from the address
 * */
#define LD_offset_ACC(processor, addr, offset)                                                     \
    (LD_offset_GENERIC(processor, accumulator, addr, offset))

/**
 * @brief Procedure that loads a value from memory at address addr+offset to the X register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The base address
 * @param[in] offset The offset from the address
 * */
#define LD_offset_X(processor, addr, offset)                                                       \
    (LD_offset_GENERIC(processor, register_x, addr, offset))

/**
 * @brief Procedure that loads a value from memory at address addr+offset to the Y register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The base address
 * @param[in] offset The offset from the address
 * */
#define LD_offset_Y(processor, addr, offset)                                                       \
    (LD_offset_GENERIC(processor, register_y, addr, offset))

/**
 * @brief Procedure that loads a value from memory at the address adr to a register
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register in which we need to load the data
 * @param[in] addr The address of the data
 * */
#define LD_absolute_GENERIC(cpu, reg, addr) LD_immediate_GENERIC(cpu, reg, cpu->mem[addr])

/**
 * @brief Procedure that loads a value from memory at the address addr to the accumulator
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The address that holds the data to load
 * */
#define LD_absolute_ACC(processor, addr) (LD_absolute_GENERIC(processor, accumulator, addr))

/**
 * @brief Procedure that loads a value from memory at the address addr to the X register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The address that holds the data to load
 * */
#define LD_absolute_X(processor, addr) (LD_absolute_GENERIC(processor, register_x, addr))

/**
 * @brief Procedure that loads a value from memory at the address addr to the Y register
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The address that holds the data to load
 * */
#define LD_absolute_Y(processor, addr) (LD_absolute_GENERIC(processor, register_y, addr))

/**
 * @brief Procedure that loads a value from memory at the address: mem[addr] | mem[addr+1]
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register in which we need to load the data
 * @param[in] addr The base address of the indirect addressing mode
 * */
#define LD_indirect_GENERIC(cpu, reg, addr) (LD_immediate_GENERIC(cpu, reg, indirect(cpu, addr)))

/**
 * @brief Procedure that lods a value from memory at the address contained in addr|addr+1.
 * (indirection)
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The base address of the indirection
 * */
#define LD_indirect_ACC(processor, addr) LD_indirect_GENERIC(processor, accumulator, addr)

/**
 * @brief Procedure that lods a value from memory at the address contained in addr|addr+1.
 * (indirection)
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The base address of the indirection
 * */
#define LD_indirect_X(processor, addr) LD_indirect_GENERIC(processor, register_x, addr)

/**
 * @brief Procedure that lods a value from memory at the address contained in addr|addr+1.
 * (indirection)
 * @param[inout] processor The CPU in which we need to load the data
 * @param[in] addr The base address of the indirection
 * */
#define LD_indirect_Y(processor, addr) LD_indirect_GENERIC(processor, register_y, addr)

/**
 * @brief Procedure that increments the register reg
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register to increment or decrement
 * @param[in] op The increment or decrement operator
 * */
#define DECINC_GENERIC(cpu, reg, op)                                                               \
    ({                                                                                             \
        op cpu->reg;                                                                               \
        if (cpu->reg == 0) {                                                                       \
            SET_Z_FLAG(cpu);                                                                       \
            CLEAR_N_FLAG(cpu);                                                                     \
        } else if (cpu->reg & BIT_7_MASK) {                                                        \
            SET_N_FLAG(cpu);                                                                       \
            CLEAR_Z_FLAG(cpu);                                                                     \
        } else {                                                                                   \
            CLEAR_N_FLAG(cpu);                                                                     \
            CLEAR_Z_FLAG(cpu);                                                                     \
        }                                                                                          \
    })

/**
 * @brief Procedure that decrement a specific memory location
 * @param[inout] processor The CPU that performs the decrement
 * @param[in] addr The address where we need to decrement
 * */
#define DEC_absolute(processor, addr) DECINC_GENERIC(processor, mem[addr], --)

/**
 * @brief Procedure that increment a specific memory location
 * @param[inout] processor The CPU that performs the increment
 * @param[in] addr The address where we need to increment
 * */
#define INC_absolute(processor, addr) DECINC_GENERIC(processor, mem[addr], ++)

/**
 * @brief Procedure that decrement a specific memory location
 * @param[inout] processor The CPU that performs the decrement
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define DEC_offset(processor, addr, offset) DECINC_GENERIC(processor, mem[addr + offset], --)

/**
 * @brief Procedure that increment a specific memory location
 * @param[inout] processor The CPU that performs the increment
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define INC_offset(processor, addr, offset) DECINC_GENERIC(processor, mem[addr + offset], ++)

/**
 * @brief Procedure that increments the X register
 * @param[inout] processor The CPU that contains the X register to increment
 * */
#define INC_X(processor) DECINC_GENERIC(processor, register_x, ++)

/**
 * @brief Procedure that increments the Y register
 * @param[inout] processor The CPU that contains the Y register to increment
 * */
#define INC_Y(processor) DECINC_GENERIC(processor, register_y, ++)

/**
 * @brief Procedure that decrements the X register
 * @param[inout] processor The CPU that contains the X register to decrement
 * */
#define DEC_X(processor) DECINC_GENERIC(processor, register_x, --)

/**
 * @brief Procedure that decrements the Y register
 * @param[inout] processor The CPU that contains the Y register to decrement
 * */
#define DEC_Y(processor) DECINC_GENERIC(processor, register_y, --)

/**
 * @brief Procedure that loads to the accumulator using Indexed Indirect
 * @param[inout] processor The CPU that holds the accumulator
 * @param[in] addr A ZERO PAGE address only !
 * @param[in] offset The offset (usually, the content of X)
 * */
#define LD_indexed_indirect_ACC(processor, addr, offset)                                           \
    (LD_immediate_GENERIC(processor, accumulator, indexed_indirect(processor, addr, offset)))

/**
 * @brief Procedure that loads to the accmulator using Inderect Indexed
 * @param[inout] processor The CPU that holds the accumulator
 * @param[in] addr A ZERO PAGE address only !
 * @param[in] offset The offset (usually, the content of Y)
 * */
#define LD_indirect_indexed_ACC(processor, addr, offset)                                           \
    (LD_immediate_GENERIC(processor, accumulator, indirect_indexed(processor, addr, offset)))

/**
 * @brief Procedure that store the register reg value to a memory address
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register that holds the value
 * @param[in] addr The address to the memory block
 * */
#define STR_absolute_GENERIC(cpu, reg, addr) ({ cpu->mem[addr] = cpu->reg; })

/**
 * @brief Procedure that store the accumulator value to a memory address
 * @param[inout] processor The CPU that holds the accumulator
 * @param[in] addr The address to the memory
 * */
#define STR_absolute_ACC(processor, addr) STR_absolute_GENERIC(processor, accumulator, addr)

/**
 * @brief Procedure that store the X register value to a memory address
 * @param[inout] processor The CPU that holds the X register
 * @param[in] addr The address to the memory
 * */
#define STR_absolute_X(processor, addr) STR_absolute_GENERIC(processor, register_x, addr)

/**
 * @brief Procedure that store the Y register value to a memory address
 * @param[inout] processor The CPU that holds the Y register
 * @param[in] addr The address to the memory
 * */
#define STR_absolute_Y(processor, addr) STR_absolute_GENERIC(processor, register_y, addr)

/**
 * @brief Procedure that store the register reg value to a memory address + offset
 * @param[inout] cpu The CPU that holds the register reg
 * @param[in] reg The register that stores the value
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define STR_offset_GENERIC(cpu, reg, addr, offset) STR_absolute_GENERIC(cpu, reg, addr + offset)

/**
 * @brief Procedure that store the accumulator value to a memory address + offset
 * @param[inout] processor The CPU that holds the accumulator
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define STR_offset_ACC(processor, addr, offset)                                                    \
    STR_offset_GENERIC(processor, accumulator, addr, offset)

/**
 * @brief Procedure that store the X register value to a memory address + offset
 * @param[inout] processor The CPU that holds the X register
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define STR_offset_X(processor, addr, offset)                                                      \
    STR_offset_GENERIC(processor, register_x, addr, offset)

/**
 * @brief Procedure that store the Y register value to a memory address + offset
 * @param[inout] processor The CPU that holds the Y register
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define STR_offset_Y(processor, addr, offset)                                                      \
    STR_offset_GENERIC(processor, register_y, addr, offset)

/**
 * @brief Procedure that store the accumulator value to a memory address using indirect indexed
 * addressing mode
 * @param[inout] processor The CPU that holds the accumulator
 * @param[in] addr A ZERO PAGE address !
 * @param[in] offset The offset (usually Y)
 * */
#define STR_indirect_indexed_ACC(processor, addr, offset)                                          \
    ({ indirect_indexed(processor, addr, offset) = processor->accumulator; })

/**
 * @brief Procedure that store the accumulator value to a memory address using indexes indirect
 * addressing mode
 * @param[inout] processor The CPU that holds the accumulator
 * @param[in] addr A ZERO PAGE address !
 * @param[in] offset The offset (usually X)
 * */
#define STR_indexed_indirect_ACC(processor, addr, offset)                                          \
    ({ indexed_indirect(processor, addr, offset) = processor->accumulator; })

/**
 * @brief Procedure that store the value in reg1 in reg2
 * @param[inout] cpu The CPU that holds both registers
 * @param[in] reg1 The register holding the value
 * @param[in] reg2 The register in which we load the value
 * */
#define TR_GENERIC(cpu, reg1, reg2)                                                                \
    ({                                                                                             \
        cpu->reg2 = cpu->reg1;                                                                     \
        LD_FLAGS_GENERIC(cpu, reg2);                                                               \
    })

/**
 * @brief Procedure that store the value in the accumulator in the X register
 * @param[inout] processor The CPU holding both registers
 * */
#define TR_ACC_X(processor) TR_GENERIC(processor, accumulator, register_x)

/**
 * @brief Procedure that store the value in accumulator in the Y register
 * @param[inout] processor The CPU holding both registers
 * */
#define TR_ACC_Y(processor) TR_GENERIC(processor, accumulator, register_y)

/**
 * @brief Procedure that store the value in the X register in the accumulator
 * @param[inout] processor The CPU holding both registers
 * */
#define TR_X_ACC(processor) TR_GENERIC(processor, register_x, accumulator)

/**
 * @brief Procedure that store the value in the Y register in the accumulator
 * @param[inout] processor The CPU holding both registers
 * */
#define TR_Y_ACC(processor) TR_GENERIC(processor, register_y, accumulator)

/**
 * @brief Procedure that store the stack pointer in the X register
 * @param[inout] processor The CPU holding both registers
 * */
#define TR_SP_X(processor) TR_GENERIC(processor, stack_pointer, register_x)

/**
 * @brief Procedure that store the value in the X register in the stack pointer
 * @param[inout] processor The CPU holding both registers
 * */
#define TR_X_SP(processor) TR_GENERIC(processor, register_x, stack_pointer)

/**
 * @brief Procedure that push the reg value to the top of the stack
 * @param[inout] cpu The CPU holding the register reg
 * @param[in] reg The register holding the value
 * */
#define PUSH_GENERIC(cpu, reg)                                                                     \
    ({                                                                                             \
        cpu->mem[STACK_END | cpu->stack_pointer] = cpu->reg;                                       \
        --(cpu->stack_pointer);                                                                    \
    })

/**
 * @brief Procedure that push an immediate value to the top of the stack
 * @param[inout] cpu The CPU holding the stack_pointer
 * @param[in] d The immediate to push
 * */
#define PUSH_immediate(cpu, d)                                                                     \
    ({                                                                                             \
        cpu->mem[STACK_END | cpu->stack_pointer] = d;                                              \
        --(cpu->stack_pointer);                                                                    \
    })

/**
 * @brief Procedure that push the accumulator value to the top of the stack
 * @param[inout] processor The CPU holding the accumulator
 * */
#define PUSH_ACC(processor) PUSH_GENERIC(processor, accumulator)

/**
 * @brief Procedure that push the processor flags to the top of the stack
 * @param[inout] processor The CPU holding the flags
 * */
#define PUSH_FLAGS(processor) PUSH_GENERIC(processor, flags)

/**
 * @brief Procedure that push the program_counter to the top of the stack
 * @param[inout] processor The CPU holding the program counter
 * */
#define PUSH_PC(processor)                                                                         \
    ({                                                                                             \
        PUSH_immediate(processor, GET_HIGHER(processor->program_counter));                         \
        PUSH_immediate(processor, GET_LOWER(processor->program_counter));                          \
    })

/**
 * @brief Procedure that pop the top value of the stack and return it to the register reg
 * @param[inout] cpu The CPU holding the register reg
 * @param[in] reg
 * */
#define PULL_GENERIC(cpu, reg)                                                                     \
    ({                                                                                             \
        ++(cpu->stack_pointer);                                                                    \
        cpu->reg = cpu->mem[0x1100 | cpu->stack_pointer];                                          \
    })

/**
 * @brief Procedure that pop the top value of the stack and return it to the accumulator
 * @param[inout] processor The CPU holding the accumulator
 * */
#define PULL_ACC(processor) PULL_GENERIC(processor, accumulator)

/**
 * @brief Procedure that pop the top value of the stack and return it to the flags register
 * @param[inout] processor The CPU holding the flags register
 * */
#define PULL_FLAGS(processor) PULL_GENERIC(processor, flags)

/**
 * @brief Procedure that pop the top value of the stack and return it to the program counter
 * @param[inout] processor The CPU holding the program counter
 * */
#define PULL_PC(processor)                                                                         \
    ({                                                                                             \
        ++(processor->stack_pointer);                                                              \
        processor->program_counter = ((processor->mem[STACK_END | processor->stack_pointer]));     \
        ++(processor->stack_pointer);                                                              \
        processor->program_counter = (processor->program_counter & 0x00ff) |                       \
                                     (processor->mem[STACK_END | processor->stack_pointer] << 8);  \
    })

/**
 * @brief Procedure that perform a ADD with the carry flag (A,Z,C,N=A+data+C) with an immediate
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] data The immediate (a byte)
 * */
#define ADD_immediate(processor, data)                                                             \
    ({                                                                                             \
        processor->cache = (processor->accumulator);                                               \
        processor->accumulator += (data + GET_C_FLAG(processor));                                  \
        if ((processor->cache & BIT_7_MASK) != (processor->accumulator & BIT_7_MASK)) {            \
            SET_V_FLAG(processor);                                                                 \
            SET_C_FLAG(processor);                                                                 \
            CLEAR_N_FLAG(processor);                                                               \
            SET_Z_FLAG(processor);                                                                 \
        } else if (processor->accumulator & BIT_7_MASK) {                                          \
            CLEAR_V_FLAG(processor);                                                               \
            SET_N_FLAG(processor);                                                                 \
            CLEAR_Z_FLAG(processor);                                                               \
        } else if (processor->accumulator == 0) {                                                  \
            SET_Z_FLAG(processor);                                                                 \
            CLEAR_V_FLAG(processor);                                                               \
            CLEAR_N_FLAG(processor);                                                               \
        } else {                                                                                   \
            CLEAR_V_FLAG(processor);                                                               \
            CLEAR_N_FLAG(processor);                                                               \
            CLEAR_Z_FLAG(processor);                                                               \
        }                                                                                          \
    })

/**
 * @brief Procedure that perform a ADD with the carry flag (A,Z,C,N=A+M+C) with absolute addressing
 * mode
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] addr The address of the data to add
 * */
#define ADD_absolute(processor, addr) ADD_immediate(processor, processor->mem[addr])

/**
 * @brief Procedure that perform a ADD with the carry flag (A,Z,C,N=A+M+C) with absolute+offset
 * addressing addressing
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset from the address
 * */
#define ADD_offset(processor, addr, offset) ADD_immediate(processor, processor->mem[addr + offset])

/**
 * @brief Procedure that perform a ADD with the carry flag (A,Z,C,N=A+M+C) with indexed indirect
 * addressing mode
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually X)
 */
#define ADD_indexed_indirect(processor, addr, offset)                                              \
    ADD_immediate(processor, indexed_indirect(processor, addr, offset))

/**
 * @brief Procedure that perform a ADD with the carry flag (A,Z,C,N=A+M+C) with indirect indexed
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually Y)
 */
#define ADD_indirect_indexed(processor, addr, offset)                                              \
    ADD_immediate(processor, indirect_indexed(processor, addr, offset))

/**
 * @brief Procedure that performs the test for every bitwise logic operations
 * @param[inout] processor The CPU that performed the bitwise operation
 * */
#define LOGICAL_FLAGS(processor)                                                                   \
    ({                                                                                             \
        if (processor->accumulator & BIT_7_MASK) {                                                 \
            CLEAR_ALL_FLAGS(processor);                                                            \
            SET_N_FLAG(processor);                                                                 \
        } else if (processor->accumulator == 0) {                                                  \
            CLEAR_ALL_FLAGS(processor);                                                            \
            SET_Z_FLAG(processor);                                                                 \
        } else {                                                                                   \
            CLEAR_ALL_FLAGS(processor);                                                            \
        }                                                                                          \
    })

/**
 * @brief Procedure that performs a bitwise logic AND (A,Z,N = A & data)
 * @param[inout] processor The CPU that performs the bitwise logic AND operation
 * @param[in] data The value to bitwise AND with the accumulator
 * */
#define AND_immediate(processor, data)                                                             \
    ({                                                                                             \
        processor->accumulator &= data;                                                            \
        LOGICAL_FLAGS(processor);                                                                  \
    })

/**
 * @brief Procedure that performs a bitwise logic AND (A,Z,N = A&M) using absolute addressing mode
 * @param[inout] processor The CPU that performs the bitwise logic AND operation
 * @param[in] addr The addressing to the data
 * */
#define AND_absolute(processor, addr) AND_immediate(processor, processor->mem[addr])

/**
 * @brief Procedure that performs a bitwise logic AND (A,Z,N = A&M) using offset addressing mode
 * @param[inout] processor The CPU that performs the bitwise logic AND operation
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define AND_offset(processor, addr, offset) AND_immediate(processor, processor->mem[addr + offset])

/**
 * @brief Procedure that perform a AND with the carry flag (A,Z,C,N=A+M+C) with indexed indirect
 * addressing mode
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually X)
 */
#define AND_indexed_indirect(processor, addr, offset)                                              \
    AND_immediate(processor, indexed_indirect(processor, addr, offset))

/**
 * @brief Procedure that perform a AND with the carry flag (A,Z,C,N=A+M+C) with indirect indexed
 * @param[inout] processor The CPU that performs the ADD with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually Y)
 */
#define AND_indirect_indexed(processor, addr, offset)                                              \
    AND_immediate(processor, indirect_indexed(processor, addr, offset))

/**
 * @brief Procedure that performs a bitwise logic OR (A,Z,N = A & data)
 * @param[inout] processor The CPU that performs the bitwise logic OR operation
 * @param[in] data The value to bitwise OR with the accumulator
 * */
#define OR_immediate(processor, data)                                                              \
    ({                                                                                             \
        processor->accumulator |= data;                                                            \
        LOGICAL_FLAGS(processor);                                                                  \
    })

/**
 * @brief Procedure that performs a bitwise logic OR (A,Z,N = A&M) using absolute addressing mode
 * @param[inout] processor The CPU that performs the bitwise logic OR operation
 * @param[in] addr The addressing to the data
 * */
#define OR_absolute(processor, addr) OR_immediate(processor, processor->mem[addr])

/**
 * @brief Procedure that performs a bitwise logic OR (A,Z,N = A&M) using offset addressing mode
 * @param[inout] processor The CPU that performs the bitwise logic OR operation
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define OR_offset(processor, addr, offset) OR_immediate(processor, processor->mem[addr + offset])

/**
 * @brief Procedure that perform a OR with the carry flag (A,Z,C,N=A+M+C) with indexed indirect
 * addressing mode
 * @param[inout] processor The CPU that performs the OR with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually X)
 */
#define OR_indexed_indirect(processor, addr, offset)                                               \
    OR_immediate(processor, indexed_indirect(processor, addr, offset))

/**
 * @brief Procedure that perform a OR with the carry flag (A,Z,C,N=A+M+C) with indirect indexed
 * @param[inout] processor The CPU that performs the OR with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually Y)
 */
#define OR_indirect_indexed(processor, addr, offset)                                               \
    OR_immediate(processor, indirect_indexed(processor, addr, offset))

/**
 * @brief Procedure that performs a bitwise logic XOR (A,Z,N = A & data)
 * @param[inout] processor The CPU that performs the bitwise logic OR operation
 * @param[in] data The value to bitwise XOR with the accumulator
 * */
#define XOR_immediate(processor, data)                                                             \
    ({                                                                                             \
        processor->accumulator ^= data;                                                            \
        LOGICAL_FLAGS(processor);                                                                  \
    })

/**
 * @brief Procedure that performs a bitwise logic XOR (A,Z,N = A&M) using absolute addressing mode
 * @param[inout] processor The CPU that performs the bitwise logic XOR operation
 * @param[in] addr The addressing to the data
 * */
#define XOR_absolute(processor, addr) XOR_immediate(processor, processor->mem[addr])

/**
 * @brief Procedure that performs a bitwise logic XOR (A,Z,N = A&M) using offset addressing mode
 * @param[inout] processor The CPU that performs the bitwise logic XOR operation
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define XOR_offset(processor, addr, offset) XOR_immediate(processor, processor->mem[addr + offset])

/**
 * @brief Procedure that perform a XOR with the carry flag (A,Z,C,N=A+M+C) with indexed indirect
 * addressing mode
 * @param[inout] processor The CPU that performs the XOR with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually X)
 */
#define XOR_indexed_indirect(processor, addr, offset)                                              \
    XOR_immediate(processor, indexed_indirect(processor, addr, offset))

/**
 * @brief Procedure that perform a XOR with the carry flag (A,Z,C,N=A+M+C) with indirect indexed
 * @param[inout] processor The CPU that performs the XOR with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually Y)
 */
#define XOR_indirect_indexed(processor, addr, offset)                                              \
    XOR_immediate(processor, indirect_indexed(processor, addr, offset))

/**
 * @brief Procedure that perform a substract with the carry flag (A,Z,C,N=A-data-C) with an
 * immediate
 * @param[inout] processor The CPU that performs the substract with Carry
 * @param[in] data The immediate (a byte)
 * */
#define SBC_immediate(processor, data)                                                             \
    ({                                                                                             \
        processor->cache = (processor->accumulator);                                               \
        processor->accumulator -= (data + GET_C_FLAG(processor));                                  \
        if ((processor->cache & BIT_7_MASK) != (processor->accumulator & BIT_7_MASK)) {            \
            SET_V_FLAG(processor);                                                                 \
            CLEAR_C_FLAG(processor);                                                               \
            CLEAR_Z_FLAG(processor);                                                               \
            SET_N_FLAG(processor);                                                                 \
        } else if (processor->accumulator & BIT_7_MASK) {                                          \
            CLEAR_V_FLAG(processor);                                                               \
            SET_N_FLAG(processor);                                                                 \
            CLEAR_Z_FLAG(processor);                                                               \
        } else if (processor->accumulator == 0) {                                                  \
            SET_Z_FLAG(processor);                                                                 \
            CLEAR_V_FLAG(processor);                                                               \
            CLEAR_N_FLAG(processor);                                                               \
        } else {                                                                                   \
            CLEAR_V_FLAG(processor);                                                               \
            CLEAR_N_FLAG(processor);                                                               \
            CLEAR_Z_FLAG(processor);                                                               \
        }                                                                                          \
    })

/**
 * @brief Procedure that perform a SBC with the carry flag (A,Z,C,N=A-M-C) with absolute addressing
 * mode
 * @param[inout] processor The CPU that performs the substract with Carry
 * @param[in] addr The address of the data to substract
 * */
#define SBC_absolute(processor, addr) SBC_immediate(processor, processor->mem[addr])

/**
 * @brief Procedure that perform a SBC with the carry flag (A,Z,C,N=A-M-C) with offset addressing
 * mode
 * @param[inout] processor The CPU that performs the substract with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define SBC_offset(processor, addr, offset) SBC_immediate(processor, processor->mem[addr + offset])

/**
 * @brief Procedure that perform a SBC with the carry flag (A,Z,C,N=A-M-C) with indexed indirect
 * addressing mode
 * @param[inout] processor The CPU that performs the substract with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually X)
 * */
#define SBC_indexed_indirect(processor, addr, offset)                                              \
    SBC_immediate(processor, indexed_indirect(processor, addr, offset))

/**
 * @brief Procedure that perform a SBC with the carry flag (A,Z,C,N=A-M-C) with indirect indexed
 * addressing mode
 * @param[inout] processor The CPU that performs the substract with Carry
 * @param[in] addr The base address
 * @param[in] offset The offset (usually Y)
 * */
#define SBC_indirect_indexed(processor, addr, offset)                                              \
    SBC_immediate(processor, indirect_indexed(processor, addr, offset))

/**
 * @brief Procedure that litteraly does nothing
 * */
#define NO_OPERATION()

/**
 * @brief Procedure that performs a BIT test (A & M, N = M7, V = M6)
 * @param[inout] processor The CPU that performs the BIT test
 * @param[in] addr The base address (ZP: 2 cycles, Absolute: 3 cycles)
 * */
#define BIT_TEST(processor, addr)                                                                  \
    ({                                                                                             \
        processor->cache = processor->accumulator & processor->mem[addr];                          \
        if (processor->cache == 0) {                                                               \
            SET_Z_FLAG(processor);                                                                 \
        } else {                                                                                   \
            CLEAR_Z_FLAG(processor);                                                               \
            if (processor->cache & BIT_7_MASK) {                                                   \
                SET_N_FLAG(processor);                                                             \
            }                                                                                      \
            if (processor->cache & BIT_6_MASK) {                                                   \
                SET_V_FLAG(processor);                                                             \
            }                                                                                      \
        }                                                                                          \
    })

/**
 * @brief Procedure that performs a BRANCH instruction for the FLAG
 * @param[inout] processor The CPU that performs the Branch instruction
 * @param[in] flag_mask The bitmask that isolate the correct flag
 * @param[in] offset The offset (a signed byte) which is added to the PC when the FLAG is SET
 * */
#define BRANCH_GENERIC_S(processor, flag_mask, offset)                                             \
    ({                                                                                             \
        if (processor->flags & flag_mask) {                                                        \
            processor->program_counter += (signed char)(offset);                                   \
        }                                                                                          \
    })
/**
 * @brief Procedure that performs a BRANCH instruction for the FLAG
 * @param[inout] processor The CPU that performs the Branch instruction
 * @param[in] flag_mask The bitmask that isolate the correct flag
 * @param[in] offset The offset (a signed byte) which is added to the PC when the FLAG is CLEAR
 * */
#define BRANCH_GENERIC_C(processor, flag_mask, offset)                                             \
    ({                                                                                             \
        if (!(processor->flags & flag_mask)) {                                                     \
            processor->program_counter += (signed char)(offset);                                   \
        }                                                                                          \
    })

/**
 * @brief Procedure that performs a BPL (branch if positive, if the N flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_If_Positive(processor, offset) BRANCH_GENERIC_C(processor, BIT_7_MASK, offset)

/**
 * @brief Procedure that performs a BVC (branch if overflow clear, if the V flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_Overflow_C(processor, offset) BRANCH_GENERIC_C(processor, BIT_6_MASK, offset)

/**
 * @brief Procedure that performs a BNE (if the Z flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_Not_Equal(processor, offset) BRANCH_GENERIC_C(processor, BIT_1_MASK, offset)

/**
 * @brief Procedure that performs a BCC (if the C flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_Carry_C(processor, offset) BRANCH_GENERIC_C(processor, BIT_0_MASK, offset)

/**
 * @brief Procedure that performs a BMI (branch if positive, if the N flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_If_Minus(processor, offset) BRANCH_GENERIC_S(processor, BIT_7_MASK, offset)

/**
 * @brief Procedure that performs a BVS (branch if positive, if the N flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_Overflow_S(processor, offset) BRANCH_GENERIC_S(processor, BIT_6_MASK, offset)

/**
 * @brief Procedure that performs a BEQ (branch if positive, if the N flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_Equal(processor, offset) BRANCH_GENERIC_S(processor, BIT_1_MASK, offset)

/**
 * @brief Procedure that performs a BCS (branch if positive, if the N flag is clear)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] offset The offset (a signed byte) which is added to the PC when the condition is true
 * */
#define BRANCH_Carry_S(processor, offset) BRANCH_GENERIC_S(processor, BIT_0_MASK, offset)

/**
 * @brief Procedure that performs a Bitshift of 1 using a specified operator and placing the
 * overflowing bit inside of the carry flag.
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] field The field of the structure that is affected by the operation
 * @param[in] operator The left or shift operator
 * @param[in] flag_shift_offset The offset to place correctly the bit inside of the Carry flag
 * @param[in] flag_mask The mask to get the correct bit
 * */
#define SHIFT_GENERIC(processor, field, operator, flag_shift_offset, flag_mask)                    \
    ({                                                                                             \
        CLEAR_C_FLAG(processor);                                                                   \
        processor->flags |= ((processor->field & flag_mask) >> (flag_shift_offset));               \
        processor->field = processor->field operator(1);                                           \
        if (processor->field == 0) {                                                               \
            SET_Z_FLAG(processor);                                                                 \
            CLEAR_N_FLAG(processor);                                                               \
        } else if (processor->field & BIT_7_MASK) {                                                \
            SET_N_FLAG(processor);                                                                 \
            CLEAR_Z_FLAG(processor);                                                               \
        }                                                                                          \
    })

/**
 * @brief Procedure that performs a bitshift to the left (ASL operation) on the accumulator and
 * place the 7th bit inside of the Carry flag.
 * @param[inout] processor The CPU that performs the operation
 * */
#define SHIFT_LEFT_ACC(processor) SHIFT_GENERIC(processor, accumulator, <<, 7, BIT_7_MASK)

/**
 * @brief Procedure that performs a bitshift to the right (LSR operation) on the accumulator and
 * place the 0th bit inside of the Carry flag.
 * @param[inout] processor The CPU that performs the operation
 * */
#define SHIFT_RIGHT_ACC(processor) SHIFT_GENERIC(processor, accumulator, >>, 0, BIT_0_MASK)

/**
 * @brief Procedure that performs a bitshift to the left (ASL operation) at the address addr and
 * place the 7th bit inside of the Carry flag
 * @param[inout] processor The CPU that performs the operation
 * @param[in] addr The base address
 * */
#define SHIFT_LEFT_absolute(processor, addr) SHIFT_GENERIC(processor, mem[addr], <<, 7, BIT_7_MASK)
/**
 * @brief Procedure that performs a bitshift to the right (LSR operation) at the address addr and
 * place the 0th bit inside of the Carry flag
 * @param[inout] processor The CPU that performs the operation
 * @param[in] addr The base address
 * */
#define SHIFT_RIGHT_absolute(processor, addr) SHIFT_GENERIC(processor, mem[addr], >>, 0, BIT_0_MASK)

/**
 * @brief Procedure that performs a bitshift to the left (ASL operation) at the address (addr +
 * offset) and place the 7th bit inside of the Carry flag
 * @param[inout] processor The CPU that performs the operation
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define SHIFT_LEFT_offset(processor, addr, offset) SHIFT_LEFT_absolute(processor, addr + offset)

/**
 * @brief Procedure that performs a bitshift to the right (LSR operation) at the address (addr +
 * offset) and place the 0th bit inside of the Carry flag
 * @param[inout] processor The CPU that performs the operation
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define SHIFT_RIGHT_offset(processor, addr, offset) SHIFT_RIGHT_absolute(processor, addr + offset)

/**
 * @brief Procedure that performs a Bitshift of 1 using a specified operator and placing the
 * overflowing bit inside of the carry flag.
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] field The field of the structure that is affected by the operation
 * @param[in] operator The left or shift operator
 * @param[in] store_offset The offset to place correctly the bit inside of the Carry flag
 * @param[in] pop_offset The offset to place correctly the bit inside of the field
 * @param[in] flag_mask The mask to get the correct bit
 * */
#define ROTATION_GENERIC(processor, field, operator, store_offset, pop_offset, flag_mask)          \
    ({                                                                                             \
        processor->cache = ((processor->field & flag_mask) >> (store_offset));                     \
        processor->field = (processor->field operator(1)) | (GET_C_FLAG(processor) << pop_offset); \
        CLEAR_C_FLAG(processor);                                                                   \
        processor->flags |= processor->cache;                                                      \
        if (processor->field == 0) {                                                               \
            SET_Z_FLAG(processor);                                                                 \
            CLEAR_N_FLAG(processor);                                                               \
        } else if (processor->field & BIT_7_MASK) {                                                \
            SET_N_FLAG(processor);                                                                 \
            CLEAR_Z_FLAG(processor);                                                               \
        }                                                                                          \
    })

/**
 * @brief Procedure that performs a bit shift with rotation to the left (ROL operation) on the
 * accumulator. Bit 0 is filled with the previous state of the C flag and then the C flag is set to
 * the current value of Bit 7.
 * @param[inout] processor The CPU that performs the instruction
 * */
#define ROTATION_LEFT_ACC(processor) ROTATION_GENERIC(processor, accumulator, <<, 7, 0, BIT_7_MASK)

/**
 * @brief Procedure that performs a bit shift with rotation to the right (ROR operation) on the
 * accumulator. Bit 7 is filled with the previous state of the C flag and then the C flag is set to
 * the current value of Bit 0.
 * @param[inout] processor The CPU that performs the instruction
 * */
#define ROTATION_RIGHT_ACC(processor) ROTATION_GENERIC(processor, accumulator, >>, 0, 7, BIT_0_MASK)

/**
 * @brief Procedure that performs a bit shift with rotation to the left (ROL operation) at the
 * memory address addr. Bit 0 is filled with the previous state of the C flag and then the C flag is
 * set to the current value of Bit 7.
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define ROTATION_LEFT_absolute(processor, addr)                                                    \
    ROTATION_GENERIC(processor, mem[addr], <<, 7, 0, BIT_7_MASK)

/**
 * @brief Procedure that performs a bit shift with rotation to the right (ROR operation) at the
 * memory address addr. Bit 7 is filled with the previous state of the C flag and then the C flag is
 * set to the current value of Bit 0.
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define ROTATION_RIGHT_absolute(processor, addr)                                                   \
    ROTATION_GENERIC(processor, mem[addr], >>, 0, 7, BIT_0_MASK)

/**
 * @brief Procedure that performs a bit shift with rotation to the left (ROL operation) at the
 * memory address (addr + offset). Bit 0 is filled with the previous state of the C flag and then
 * the C flag is set to the current value of Bit 7.
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define ROTATION_LEFT_offset(processor, addr, offset)                                              \
    ROTATION_LEFT_absolute(processor, addr + offset)

/**
 * @brief Procedure that performs a bit shift with rotation to the right (ROR operation) at the
 * memory address (addr + offset). Bit 7 is filled with the previous state of the C flag and then
 * the C flag is set to the current value of Bit 0.
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * @param[in] offset The offset from the base address
 * */
#define ROTATION_RIGHT_offset(processor, addr, offset)                                             \
    ROTATION_RIGHT_absolute(processor, addr + offset)

/**
 * @brief Procedure that sets the program counter to a certain address (JMP instruction)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define JUMP_absolute(processor, addr) processor->program_counter = addr

/**
 * @brief Procedure that sets the program counter to a certain indirect address (JMP instruction)
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define JUMP_indirect(processor, addr)                                                             \
    JUMP_absolute(processor, ((cpu->mem[(addr)]) | (cpu->mem[(addr) + 1] << 8)))

/**
 * @brief Procedure that performs a generic IR instruction.
 * @param[inout] processor The CPU that performs the interrupt
 * @param[in] vector_start The first address of the interrupt's vector
 * */
#define GENERIC_IR(processor, vector_start)                                                        \
    ({                                                                                             \
        PUSH_PC(processor);                                                                        \
        PUSH_FLAGS(processor);                                                                     \
        JUMP_indirect(processor, vector_start);                                                    \
    })

/**
 * @brief Procedure that performs a break instruction (forces an interrupt).
 * @param[inout] processor The CPU that performs the interrupt
 * */
#define BREAK(processor)                                                                           \
    ({                                                                                             \
        GENERIC_IR(processor, 0xfffe);                                                             \
        SET_B_FLAG(processor);                                                                     \
    })

/**
 * @brief Procedure that performs an interrupt request (when an IRQ is pending).
 * @param[inout] processor The CPU that performs the interrupt
 * */
#define IRQ(processor)                                                                             \
    ({                                                                                             \
        GENERIC_IR(processor, 0xfffe);                                                             \
        CLEAR_B_FLAG(processor);                                                                   \
    })

/**
 * @brief Procedure that performs a non maskable interrupt (when an NMI is pending).
 * @param[inout] processor The CPU that performs the interrupt
 * */
#define NMI(processor)                                                                             \
    ({                                                                                             \
        GENERIC_IR(processor, 0xfffa);                                                             \
        CLEAR_B_FLAG(processor);                                                                   \
    })

/**
 * @brief Procedure that performs a NES reset interrupt
 * @param[inout] processor The CPU that performs the interrupt
 * */
#define RESET(processor)                                                                           \
    ({                                                                                             \
        processor->stack_pointer = 0xff;                                                           \
        CLEAR_ALL_FLAGS(processor);                                                                \
        JUMP_indirect(processor, 0xfffc);                                                          \
    })

/**
 * @brief Procedure that performs a Jump to a subroutine (JSR instruction) located at the address
 * addr
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The address of the subroutine
 * */
#define JUMP_Subroutine(processor, addr)                                                           \
    ({                                                                                             \
        processor->program_counter += 2;                                                           \
        PUSH_PC(processor);                                                                        \
        JUMP_absolute(processor, addr);                                                            \
    })

/**
 * @brief Procedure that performs a return from subroutine (RTS instruction) by pulling the top two
 * elements of the stack as the PC (ad then, it increments the PC to prevent some errors)
 * @param[inout] processor The CPU that performs the instruction
 * */
#define RETURN_Subroutine(processor)                                                               \
    ({                                                                                             \
        PULL_PC(processor);                                                                        \
        ++processor->program_counter;                                                              \
    })

/**
 * @brief Procedure that performs a return from interrupt (RTI instruction) by pulling the flags
 * from the top of the stack and then pulling the PC.
 * @param[inout] processor The CPU that performs the instruction
 * */
#define RETURN_Interrupt(processor)                                                                \
    ({                                                                                             \
        PULL_FLAGS(processor);                                                                     \
        PULL_PC(processor);                                                                        \
    })

/**
 * @brief Procedure that performs a CMP operation (Z,C,N = reg - M) with an immediate value
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] value The current value
 * @param[in] register The register used for the comparison
 * */
#define COMPARE_GENERIC_Immediate(processor, value, register)                                      \
    ({                                                                                             \
        if (processor->register > value) {                                                         \
            SET_C_FLAG(processor);                                                                 \
            CLEAR_Z_FLAG(processor);                                                               \
            CLEAR_N_FLAG(processor);                                                               \
        } else if (processor->register == value) {                                                 \
            SET_Z_FLAG(processor);                                                                 \
            SET_C_FLAG(processor);                                                                 \
            CLEAR_N_FLAG(processor);                                                               \
        } else {                                                                                   \
            CLEAR_Z_FLAG(processor);                                                               \
            CLEAR_C_FLAG(processor);                                                               \
            SET_N_FLAG(processor);                                                                 \
        }                                                                                          \
    })

/**
 * @brief Procedure that performs a CMP operation (Z,C,N = A - M) with an immediate value
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] value The current value
 * */
#define COMPARE_Immediate_ACC(processor, value)                                                    \
    COMPARE_GENERIC_Immediate(processor, value, accumulator)

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = A - M) with an absolute addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define COMPARE_absolute_ACC(processor, addr) COMPARE_Immediate_ACC(processor, processor->mem[addr])

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = A - M) with an absolute + offset
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * @param[in] offset The offset
 * */
#define COMPARE_offset_ACC(processor, addr, offset) COMPARE_absolute_ACC(processor, addr + offset)

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = A - M) with an indexed indirect
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * @param[in] offset The offset
 * */
#define COMPARE_indexed_indirect_ACC(processor, addr, offset)                                      \
    COMPARE_absolute_ACC(processor, indexed_indirect(processor, addr, offset))

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = A - M) with an indirect indexed
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * @param[in] offset The offset
 * */
#define COMPARE_indirect_indexed_ACC(processor, addr, offset)                                      \
    COMPARE_absolute_ACC(processor, indirect_indexed(processor, addr, offset))

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = X - M) with a value
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] value The immediate value
 * */
#define COMPARE_Immediate_X(processor, value)                                                      \
    COMPARE_GENERIC_Immediate(processor, value, register_x)

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = X - M) with an indirect indexed
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define COMPARE_absolute_X(processor, addr) COMPARE_Immediate_X(processor, processor->mem[addr])

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = Y - M) with a value
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] value The immediate value
 * */
#define COMPARE_Immediate_Y(processor, value)                                                      \
    COMPARE_GENERIC_Immediate(processor, value, register_y)

/**
 * @brief Procedure that performs a CMP operation (Z, C, N = Y - M) with an indirect indexed
 * addressing mode
 * @param[inout] processor The CPU that performs the instruction
 * @param[in] addr The base address
 * */
#define COMPARE_absolute_Y(processor, addr) COMPARE_Immediate_X(processor, processor->mem[addr])

#endif // !cpu_h
