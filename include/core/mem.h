#ifndef mem_h
#define mem_h

#include "core/types.h"

typedef data* memory;

/**
 * @brief This function allocate on the heap a memory of exactly TOTAL_MEMORY_SIZE
 * @return The memory pointer
 * */
memory init_memory();

/**
 * @brief This procedure set the entire memory to 0.
 * @param[inout] mem The memory pointer
 * */
void reset_memory(memory mem);

/**
 * @brief This procedure free the memory pointer
 * @param[in] mem The memory pointer
 * */
void free_memory(memory mem);

/**
 * @brief This procedure make the memory mirroring
 * @param[inout] mem The memory pointer
 * */
void memory_mirroring(memory mem);

#endif // !mem_h
