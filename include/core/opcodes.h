#ifndef opcodes_h
#define opcodes_h

enum CPU_INSTRUCTION {
    // ADC : Add with carry
    ADC_I = 0x69,   ///< ADC Immediate
    ADC_Z = 0x65,   ///< ADC Zero Page
    ADC_ZX = 0x75,  ///< ADC Zero Page, X
    ADC_A = 0x6d,   ///< ADC Absolute
    ADC_AX = 0x7d,  ///< ADC Absolute, X
    ADC_AY = 0x79,  ///< ADC Absolute, Y
    ADC_IdX = 0x61, ///< ADC (Indirect, X)
    ADC_IdY = 0x71, ///< ADC (Indirect), Y

    // AND: Logic AND

    AND_I = 0x29,   ///< AND Immediate
    AND_Z = 0x25,   ///< AND Zero Page
    AND_ZX = 0x35,  ///< AND Zero Page, X
    AND_A = 0x2d,   ///< AND Absolute
    AND_AX = 0x3d,  ///< AND Absolute, X
    AND_AY = 0x39,  ///< AND Absolute, Y
    AND_IdX = 0x21, ///< AND (Indirect, X)
    AND_IdY = 0x31, ///< AND (Indirect), Y

    // ASL: Arithmetic Shift Left

    ASL_Ac = 0x0a, ///< ASL Accumulator
    ASL_Z = 0x06,  ///< ASL Zero Page
    ASL_ZX = 0x16, ///< ASL Zero Page, X
    ASL_A = 0x0e,  ///< ASL Absolute
    ASL_AX = 0x1e, ///< ASL Absolute, X

    // BCC: Branch if Carry Clear
    BCC = 0x90, ///< If C = 0

    // BCS: Branch if Carry Set
    BCS = 0xb0, ///< If C = 1

    // BEQ: Branch if Equal
    BEQ = 0xf0, ///< If Z = 1

    // BIT: Bit Test

    BIT_Z = 0x24, ///< BIT Zero Page
    BIT_A = 0x2c, ///< BIT Absolute

    // BMI: Branch if Minus
    BMI = 0x30, ///< iF N = 1

    // BNE: Branch if Not Equal
    BNE = 0xd0, ///< If Z = 0

    // BPL: Branch if Positive
    BPL = 0x10, ///< If N = 0

    // BRK: Break
    BRK = 0x00, ///< Break instruction

    // BVC: Branch if Overflow Clear
    BVC = 0x50, ///< If V = 0

    // BVS: Branch if Overflow Set
    BVS = 0x70, ///< If V = 1

    // CLC: Clear Carry flag
    CLC = 0x18, ///< C = 0

    // CLD: Clear Decimal flag
    CLD = 0xd8, ///< D = 0

    // CLI: Clear Interrupt Disable flag
    CLI = 0x58, ///< I = 0

    // CLV: Clear Overflow flag
    CLV = 0xb8, ///< V = 0

    // CMP: Compare Accumulator
    CMP_I = 0xc9,   ///< CMP Immediate
    CMP_Z = 0xc5,   ///< CMP Zero Page
    CMP_ZX = 0xd5,  ///< CMP Zero Page, X
    CMP_A = 0xcd,   ///< CMP Absolute
    CMP_AX = 0xdd,  ///< CMP Absolute, X
    CMP_AY = 0xd9,  ///< CMP Absolute, Y
    CMP_IdX = 0xc1, ///< CMP (Indirect, X)
    CMP_IdY = 0xd1, ///< CMP (Indirect), Y

    // CPX: Compare X Register
    CPX_I = 0xe0, ///< CPX Immediate
    CPX_Z = 0xe4, ///< CPX Zero Page
    CPX_A = 0xec, ///< CPX Absolute

    // CPY: Compare Y Register
    CPY_I = 0xc0, ///< CPY Immediate
    CPY_Z = 0xc4, ///< CPY Zero Page
    CPY_A = 0xcc, ///< CPY Absolute

    // DEC: Decrement Memory
    DEC_Z = 0xc6,  ///< DEC Zero Page
    DEC_ZX = 0xd6, ///< DEC Zero Page, X
    DEC_A = 0xce,  ///< DEC Absolute
    DEC_AX = 0xde, ///< DEC Absolute, X

    // DEX: Decrement X Register
    DEX = 0xca, ///< X = X - 1

    // DEY: Decrement Y Register
    DEY = 0x88, ///< Y = Y - 1

    // EOR: Exclusive OR
    EOR_I = 0x49,   ///< EOR Immediate
    EOR_Z = 0x45,   ///< EOR Zero Page
    EOR_ZX = 0x55,  ///< EOR Zero Page, X
    EOR_A = 0x4d,   ///< EOR Abolute
    EOR_AX = 0x5d,  ///< EOR Absolute, X
    EOR_AY = 0x59,  ///< EOR Absolute, Y
    EOR_IdX = 0x41, ///< EOR (Indirect, X)
    EOR_IdY = 0x51, ///< EOR (Indirect), Y

    // INC: Increment Memory
    INC_Z = 0xe6,  ///< INC Zero Page
    INC_ZX = 0xf6, ///< INC Zero Page, X
    INC_A = 0xee,  ///< INC Absolute
    INC_AX = 0xfe, ///< INC Absolute, X

    // INX: Increment X Register
    INX = 0xe8, ///< X = X + 1

    // INY: Increment Y Register
    INY = 0xc8, ///< Y = Y + 1

    // JMP: Jump instruction
    JMP_A = 0x4c,  ///< JMP Absolute
    JMP_Id = 0x6c, ///< JMP Indirect

    // JSR: Jump Subroutine
    JSR = 0x20, ///< Jump to the subroutine

    // LDA: Load Accumulator
    LDA_I = 0xa9,   ///< LDA Immediate
    LDA_Z = 0Xa5,   ///< LDA Zero Page
    LDA_ZX = 0xb5,  ///< LDA Zero Page, X
    LDA_A = 0xad,   ///< LDA Absolute
    LDA_AX = 0xbd,  ///< LDA Absolute, X
    LDA_AY = 0xb9,  ///< LDA Absolute, Y
    LDA_IdX = 0xa1, ///< LDA (Indirect, X)
    LDA_IdY = 0xb1, ///< LDA (Indirect), Y

    // LDX: Load X Register
    LDX_I = 0xa2,  ///< LDX Immediate
    LDX_Z = 0xa6,  ///< LDX Zero Page
    LDX_ZY = 0xb6, ///< LDX Zero Page, Y
    LDX_A = 0xae,  ///< LDX Absolute
    LDX_AY = 0xbe, ///< LDX Absolute, Y

    // LDY: Load Y Register
    LDY_I = 0xa0,  ///< LDY Immediate
    LDY_Z = 0xa4,  ///< LDY Zero Page
    LDY_ZX = 0xb4, ///< LDY Zero Page, X
    LDY_A = 0xac,  ///< LDY Absolute
    LDY_AX = 0xbc, ///< LDY Absolute, X

    // LSR: Logical Shift Right
    LSR_Ac = 0x4a, ///< LSR Accumulator
    LSR_Z = 0x46,  ///< LSR Zero Page
    LSR_ZX = 0x56, ///< LSR Zero Page, X
    LSR_A = 0x4e,  ///< LSR Absolute
    LSR_AX = 0x5e, ///< LSR Absolute, X

    // NOP: No Operation
    NOP = 0xea, ///< No Operation

    // ORA: Logical OR
    ORA_I = 0x09,   ///< ORA Immediate
    ORA_Z = 0x05,   ///< ORA Zero Page
    ORA_ZX = 0x15,  ///< ORA Zero Page, X
    ORA_A = 0x0d,   ///< ORA Absolute
    ORA_AX = 0x1d,  ///< ORA Absolute, X
    ORA_AY = 0x19,  ///< ORA Absolute, Y
    ORA_IdX = 0x01, ///< ORA (Indirect, X)
    ORA_IdY = 0x11, ///< ORA (Indirect), Y

    // PHA: Push Accumulator
    PHA = 0x48, ///< push(ACC); sp = sp - 1;

    // PHP: Push Processor Status
    PHP = 0x08, ///< push(flags); sp = sp - 1;

    // PLA: Pull Accumulator
    PLA = 0x68, ///< ACC = pull(); sp = sp + 1;

    // PLP: Pull Processor Status
    PLP = 0x28, ///< flags = pull(); sp = sp + 1;

    // ROL: Rotate Left
    ROL_Ac = 0x2a, ///< ROL Accumulator
    ROL_Z = 0x26,  ///< ROL Zero Page
    ROL_ZX = 0x36, ///< ROL Zero Page, X
    ROL_A = 0x2e,  ///< ROL Absolute
    ROL_AX = 0x3e, ///< ROL Absolute, X

    // ROR: Rotate Right
    ROR_Ac = 0x6a, ///< ROR Accumulator
    ROR_Z = 0x66,  ///< ROR Zero Page
    ROR_ZX = 0x76, ///< ROR Zero Page, X
    ROR_A = 0x6e,  ///< ROR Absolute
    ROR_AX = 0x7e, ///< ROR Absolute, X

    // RTI: Return from Interrupt
    RTI = 0x40, ///< Return from the current interrupt

    // RTS: Return from subroutine
    RTS = 0x60, ///< Return from the current subroutine

    // SBC: Substract with Carry
    SBC_I = 0xe9,   ///< SBC Immediate
    SBC_Z = 0xe5,   ///< SBC Zero Page
    SBC_ZX = 0xf5,  ///< SBC Zero Page, X
    SBC_A = 0xed,   ///< SBC Absolute
    SBC_AX = 0xfd,  ///< SBC Absolute, X
    SBC_AY = 0xf9,  ///< SBC Absolute, Y
    SBC_IdX = 0xe1, ///< SBC (Indirect, X)
    SBC_IdY = 0xf1, ///< SBC (Indirect), Y

    // SEC: Set carry flag
    SEC = 0x38, ///< C = 1

    // SED: Set Decimal flag
    SED = 0xf8, ///< D = 1

    // SEI: Set Interrupt disable
    SEI = 0x78, ///< I = 1

    // STA: Store Accumulator
    STA_Z = 0x85,   ///< STA Zero Page
    STA_ZX = 0x95,  ///< STA Zero Page, X
    STA_A = 0x8d,   ///< STA Absolute
    STA_AX = 0x9d,  ///< STA Absolute, X
    STA_AY = 0x99,  ///< STA Absolute, Y
    STA_IdX = 0x81, ///< STA (Indirect, X)
    STA_IdY = 0x91, ///< STA (Indirect), Y

    // STX: Store X Register
    STX_Z = 0x86,  ///< STX Zero Page
    STX_ZY = 0x96, ///< STX Zero Page, Y
    STX_A = 0x8e,  ///< STX Absolute

    // STY: Store Y Register
    STY_Z = 0x84,  ///< STY Zero Page
    STY_ZX = 0x94, ///< STY Zero Page, X
    STY_A = 0x8c,  ///< STY Absolute

    // TAX: Transfer Accumulator to X
    TAX = 0xaa, ///< X = ACC

    // TSX: Transfer stack pointer to X
    TSX = 0xba, ///< X = sp

    // TXA: Transfer X to the Accumulator
    TXA = 0x8a, ///< ACC = X

    // TYA: Transfer Y to the Accumulator
    TYA = 0x98, ///< ACC = Y

    // TXS: Transfer X to stack pointer
    TXS = 0x9a,

    // TAY: Transfer Accumulator to Y
    TAY = 0xa8,
};

#endif // !opcodes_h
