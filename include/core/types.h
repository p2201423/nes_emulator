#ifndef types_h
#define types_h

#include <stdint.h>

typedef uint8_t byte;              ///< Nickname for uint8_t
typedef uint16_t two_bytes_word;   ///< Nickname for uint16_t
typedef uint32_t four_bytes_word;  ///< Nickname for uint32_t
typedef uint64_t eight_bytes_word; ///< Nickname for uint64_t

typedef byte data;              ///< Definition of the data format used by the CPU
typedef two_bytes_word address; ///< Definition of the address format used by the CPU

typedef int16_t sbuffer_t; ///< Definition of the sound buffer type used by the APU
typedef byte errcode_t;    ///< Definition of the error code type used by some function

typedef float duty_cycle_t; ///< Definition of the duty cycle used by the APU

typedef enum : byte {
    SQUARE = 0,      ///< Rectangle/Square waveform (based on duty cycle)
    TRIANGLE = 1,    ///< Triangle waveform
    WHITE_NOISE = 2, ///< White noise waveform
    METAL_NOISE = 3, ///< Metal noise waveform
} wave_t;            ///< Definition of a waveform type used by the APU

#endif // !types_h
