#ifndef macros_h
#define macros_h

#include <stddef.h>

// Some Constants
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

// #define NULL ((void*)0)

#define NES_SIGNATURE 0x4e45531a

// Note: 65536 = 64KiB
#define TOTAL_MEM_SIZE 65536

// Error codes

#define ERR_ABN_OPCODE 0xff

// Some bit mask on 8-bits
#define BIT_7_MASK 0b10000000
#define BIT_6_MASK 0b01000000
#define BIT_5_MASK 0b00100000
#define BIT_4_MASK 0b00010000
#define BIT_3_MASK 0b00001000
#define BIT_2_MASK 0b00000100
#define BIT_1_MASK 0b00000010
#define BIT_0_MASK 0b00000001

#define NBIT_7_MASK 0b01111111
#define NBIT_6_MASK 0b10111111
#define NBIT_5_MASK 0b11011111
#define NBIT_4_MASK 0b11101111
#define NBIT_3_MASK 0b11110111
#define NBIT_2_MASK 0b11111011
#define NBIT_1_MASK 0b11111101
#define NBIT_0_MASK 0b11111110

// Get lower of higher part of a 16-bits word

#define GET_LOWER(x) (x & 0x00FF)
#define GET_HIGHER(x) ((x >> 8) & 0x00FF)

// Remarkable pointers

#define STACK_START 0x11FF
#define STACK_END 0x1100

#define RAM_START 0x0000
#define RAM_END 0x07FF

#define RAM_MIRROR_1_START 0x0800
#define RAM_MIRROR_1_END 0x0FFF

#define RAM_MIRROR_2_START 0x1000
#define RAM_MIRROR_2_END 0x17FF

#define RAM_MIRROR_3_START 0x1800
#define RAM_MIRROR_3_END 0x1FFF

#define IO_START 0x2000

#define PPU_REG_ZERO 0x2000
#define PPU_REG_ONE 0x2001
#define PPU_REG_TWO 0x2002
#define PPU_REG_THREE 0x2003
#define PPU_REG_FOUR 0x2004
#define PPU_REG_FIVE 0x2005
#define PPU_REG_SIX 0x2006
#define PPU_REG_SEVEN 0x2007

#define PPU_REG_START PPU_REG_ZERO
#define PPU_REG_END PPU_REG_SEVEN

// Note: the PPU mirrors are repeated every 8 bytes between the address below
#define PPU_MIRRORS_START_ZERO 0x2008
#define PPU_MIRRORS_START_ONE 0x2009
#define PPU_MIRRORS_START_TWO 0x200a
#define PPU_MIRRORS_START_THREE 0x200b
#define PPU_MIRRORS_START_FOUR 0x200c
#define PPU_MIRRORS_START_FIVE 0x200d
#define PPU_MIRRORS_START_SIX 0x200e
#define PPU_MIRRORS_START_SEVEN 0x200f

#define PPU_MIRRORS_END 0x3FFF

#define APU_REG_START 0x4000
#define APU_SQUARE_ONE_START APU_REG_START

// DDLC VVVV | D duty time (form of the wave) | L looping | C Constant_vol ? | V: MultiPurpose
// DT: 12.5% (sequence: 0 1 0 0 0 0 0 0) | 25% (0 1 1 0 0 0 0 0) | 50% (0 1 1 1 1 0 0 0) | -25% (1 0
// 0 1 1 1 1 1)
// If not(L) && not(C) => V=length of the note (works like an envelope)
// If L && not(C) => V=how fast the note is repeated
// If not(L) && C => V=volume of the channel
// If L && C => V=volume of the channel & note repeat infinitly until we change the value at x4000
#define APU_SQUARE_ONE_CONF APU_SQUARE_ONE_START

// EPPP NSSS | E Enable frequency sweep | P (smaller = faster sweep) | N 0=Sweep down 1=Sweep up
// S (smaller = faster sweep)
#define APU_SQUARE_ONE_FREQ_SWEEP 0x4001

// TTTT TTTT | T lower byte of the frequency
#define APU_SQUARE_ONE_FREQ_LOW 0x4002

// LLLL LTTT | L Length of the note (assuming it is not played constantly)
// WARNING: 0000 1 => very very long note
// T 3 high bits of the note's frequency
#define APU_SQUARE_ONE_FREQ_HIGH 0x4003

// The second square channel works exactly in the same way
#define APU_SQUARE_TWO_START 0x4004
#define APU_SQUARE_TWO_CONF APU_SQUARE_TWO_START
#define APU_SQUARE_TWO_FREQ_SWEEP 0x4005
#define APU_SQUARE_TWO_FREQ_LOW 0x4006
#define APU_SQUARE_TWO_FREQ_HIGH 0x4007

#define APU_TRIANGLE_START 0x4008

// CRRR RRRR | C = constant note
// R changes the length of the note
// However, its behavior is rly strange. For instance: $FF = constant note | $80 = channel off
// $7F = length controlled by the register 0x400b
#define APU_TRIANGLE_CONF APU_TRIANGLE_START

#define APU_TRIANGLE_UNUSED 0x4009

// TTTT TTTT | T lower byte of the frequency
#define APU_TRIANGLE_FREQ_LOW 0x400a

// LLLL LTTT | L length of the note if triangle_conf = $7F
// T 3 high bits of the note's frequency
#define APU_TRIANGLE_FREQ_HIGH 0x400b

#define APU_NOISE_START 0x400c

// --LC VVVV | Like square channel but without a duty time
#define APU_NOISE_CONF APU_NOISE_START

#define APU_NOISE_UNUSED 0x400d

// Z--- TTTT | Z 1=metal noise 0=white noise
// T lower value => higher pitch (highest pitch when TTTT = 0)
#define APU_NOISE_FREQUENCY 0x400e

// LLLL L--- | L length of the noise
#define APU_NOISE_LENGTH 0x400f

#define APU_DMC_START 0x4010

// IL-- RRRR | I sets DMC triggers IRQs | L loop sample | R sample rate
#define APU_DMC_CONF APU_DMC_START

// -DDD DDDD | D starting volume of the sample
#define APU_DMC_VOLUME 0x4011

// AAAA AAAA | address to the sample
// Note: %11AAAAAA AA000000
// Therefore: Lowest Sample addr: $C000 | Highest $FFC0
#define APU_DMC_SAMPLE_ADDR 0x4012

// LLLL LLLL | L length of the sample
#define APU_DMC_LENGTH 0x4013

#define APU_DMC_UNUSED 0x4014

// ---D TN21 | D DMC | T triangle | N noise | 2 square2 | 1 square1
// 1 to enable | 0 to disable
#define APU_CONTROL_WRITE 0x4015

// IF-D NT21 | I DMC interrupt | F frame input
// The rest are the same as above (1 is enabled, 0 if not)
#define APU_CONTROL_READ APU_CONTROL_WRITE

#define APU_UNSUSED 0x4016

// SD-- ---- | S 5-frame sequence | D disable frame interrupts
// Mode 0: 4-frame sequence
// Mode 1: 5-frame sequence
// Note: see https://www.nesdev.org/wiki/APU_Frame_Counter , basically, it changes the clock rate of
// the APU
#define APU_FRAME_COUNTER 0x4017
#define APU_REG_END APU_FRAME_COUNTER

#define TEST_MODE_IO_REG_START 0x4018
#define TEST_MODE_IO_REG_END 0x401F

#define IO_END 0x401F

#define CARTRIDGE_START 0x4020

#define CARTRIDGE_RAM_START 0x6000
#define CARTRIDGE_RAM_END 0x7FFF

#define GAME_ROM_START 0x8000

#define APU_DCM_START 0xC000
#define APU_DCM_END 0xFFF1

#define GAME_ROM_END 0xFFFF

#define CARTRIDGE_END 0xFFFF

// Interrupt vector pointers

#define NMI_HIGH 0xFFFA
#define NMI_LOW 0xFFFB

#define RESET_HIGH 0xFFFC
#define RESET_LOW 0xFFFD

#define IRQ_BRK_HIGH 0xFFFE
#define IRQ_BRK_LOW 0xFFFF

#endif // !macros_h
