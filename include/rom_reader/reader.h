#ifndef reader_h
#define reader_h

#include <sys/types.h>

typedef unsigned char* rom_buffer;

typedef struct {
    rom_buffer buff;  ///< The buffer that contains all the ROM's data
    size_t buff_size; ///< The size of the buffer
} rom;

/**
 * @brief A function that open the NES rom at a specified path
 * @param[in] filepath The path to the rom
 * @return A pointer to the buffer that contains the NES rom
 * */
rom* open_rom(char* filepath);

/**
 * @brief Procedure that displays the content of the rom in hexadecimal.
 * @param[in] r The ROM to display
 * */
void display_rom(rom* r);

/**
 * @brief Frees the rom struct
 * @param[in] r The rom to free
 * */
void free_rom(rom* r);

#endif // reader_h
