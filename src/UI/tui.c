#include "UI/tui.h"

void init_nscreen(const char* local_format) {
    setlocale(LC_ALL, local_format);
    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    // nodelay(stdscr, TRUE);
}

void printw_cpu_state(CPU* cpu) {
    printw("\nACC: %i\tX: %i\t Y: %i\n", cpu->accumulator, cpu->register_x, cpu->register_y);
    printw("N: %i\tV: %i\tP: %i\tB: %i\n", GET_N_FLAG(cpu) >> 7, GET_V_FLAG(cpu) >> 6,
           GET_P_FLAG(cpu) >> 5, GET_B_FLAG(cpu) >> 4);
    printw("D: %i\tI: %i\tZ: %i\tC: %i\n", GET_D_FLAG(cpu) >> 3, GET_I_FLAG(cpu) >> 2,
           GET_Z_FLAG(cpu) >> 1, GET_C_FLAG(cpu));
    printw("SP: %#02x\tPC: %#02x\n", cpu->stack_pointer, cpu->program_counter);
}

void end_nscreen() {
    refresh();
    getch();
    endwin();
}
