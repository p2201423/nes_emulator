#include "core/cpu.h"
#include "core/opcodes.h"
#include "core/types.h"
#include "macros.h"

#include <stdio.h>
#include <stdlib.h>

void init_cpu(CPU* cpu, memory mem) {
    cpu->program_counter = 0xFFFC;
    cpu->stack_pointer = 0xFF;
    cpu->accumulator = 0x00;
    cpu->register_x = 0x00;
    cpu->register_y = 0x00;
    cpu->flags = 0x00;
    cpu->mem = mem;
    cpu->cache = 0x00;
    cpu->cycle_counter = 0x00;
}

void free_cpu(CPU* cpu) {
    free_memory(cpu->mem);
    free(cpu);
    cpu = ((void*)0);
}

unsigned char get_next_instruction(CPU* cpu) {

    switch (cpu->mem[cpu->program_counter]) {

    case ADC_A:

    case ADC_AX:

    case ADC_AY:

    case AND_A:

    case AND_AX:

    case AND_AY:

    case ASL_A:

    case ASL_AX:

    case BIT_A:

    case CMP_A:

    case CMP_AX:

    case CMP_AY:

    case CPX_A:

    case CPY_A:

    case DEC_A:

    case DEC_AX:

    case EOR_A:

    case EOR_AX:

    case EOR_AY:

    case INC_A:

    case INC_AX:

    case JMP_A:

    case JMP_Id:

    case JSR:

    case LDA_A:

    case LDA_AX:

    case LDA_AY:

    case LDX_A:

    case LDX_AY:

    case LDY_A:

    case LDY_AX:

    case LSR_A:

    case LSR_AX:

    case ORA_A:

    case ORA_AX:

    case ORA_AY:

    case ROL_A:

    case ROL_AX:

    case ROR_A:

    case ROR_AX:

    case SBC_A:

    case SBC_AX:

    case SBC_AY:

    case STA_A:

    case STA_AX:

    case STA_AY:

    case STX_A:

    case STY_A:
        cpu->program_counter += 3;
        break;

    case BRK:

    case CLC:

    case CLD:

    case CLI:

    case CLV:

    case DEX:

    case DEY:

    case INX:

    case INY:

    case LSR_Ac:

    case NOP:

    case PHP:

    case PHA:

    case PLP:

    case PLA:

    case ROR_Ac:

    case ROL_Ac:

    case RTI:

    case RTS:

    case SEC:

    case SED:

    case SEI:

    case TAX:

    case TXA:

    case TAY:

    case TYA:

    case TSX:

    case TXS:
        cpu->program_counter += 1;
        break;

    default:
        cpu->program_counter += 2;
        break;
    }

    unsigned char cycles = 0;

    switch (cpu->mem[cpu->program_counter]) {

    case TAX:

    case TAY:

    case TSX:

    case TXA:

    case TXS:

    case TYA:

    case SEC:

    case SED:

    case SEI:

    case SBC_I:

    case ROR_Ac:

    case ROL_Ac:

    case ORA_I:

    case NOP:

    case LSR_Ac:

    case LDY_I:

    case LDA_I:

    case ASL_Ac:

    case AND_I:

    case LDX_I:

    case CLD:

    case CLI:

    case CLV:

    case CPX_I:

    case CPY_I:

    case CMP_I:

    case DEX:

    case DEY:

    case EOR_I:

    case INX:

    case INY:

    case ADC_I:
        cycles = 2;
        break;

    case STX_Z:

    case STA_Z:

    case SBC_Z:

    case PHA:

    case PHP:

    case ORA_Z:

    case LDX_Z:

    case LDA_Z:

    case JMP_A:

    case EOR_Z:

    case CPY_Z:

    case CPX_Z:

    case CMP_Z:

    case AND_Z:

    case ADC_Z:

    case LDY_Z:
        cycles = 3;
        break;

    case STX_A:

    case STX_ZY:

    case STA_ZX:

    case STA_A:

    case SBC_ZX:

    case SBC_A:

    case PLA:

    case PLP:

    case LDY_ZX:

    case LDY_A:

    case LDA_ZX:

    case LDA_A:

    case EOR_ZX:

    case EOR_A:

    case CPY_A:

    case CPX_A:

    case CMP_ZX:

    case CMP_A:

    case LDX_ZY:

    case LDX_A:

    case AND_ZX:

    case ADC_ZX:

    case AND_A:

    case ORA_ZX:

    case ORA_A:

    case ADC_A:
        cycles = 4;
        break;

    case STA_AX:

    case STA_AY:

    case ROR_Z:

    case ROL_Z:

    case LSR_Z:

    case JMP_Id:

    case INC_Z:

    case DEC_Z:

    case ASL_Z:
        cycles = 5;
        break;

    case SBC_AX:

    case LDA_AX:

    case EOR_AX:

    case CMP_AX:

    case AND_AX:

    case LDY_AX:

    case ORA_AX:

    case ADC_AX:
        if (is_page_crossed(get_address(cpu), cpu->register_x)) {
            cycles = 5;
        } else {
            cycles = 4;
        }
        break;

    case SBC_AY:

    case ORA_AY:

    case LDA_AY:

    case EOR_AY:

    case CMP_AY:

    case AND_AY:

    case LDX_AY:

    case ADC_AY:
        if (is_page_crossed(get_address(cpu), cpu->register_y)) {
            cycles = 5;
        } else {
            cycles = 4;
        }
        break;

    case RTI:

    case RTS:

    case ROR_ZX:

    case ROR_A:

    case ROL_ZX:

    case ROL_A:

    case LSR_ZX:

    case LSR_A:

    case LDA_IdX:

    case JSR:

    case INC_ZX:

    case INC_A:

    case EOR_IdX:

    case DEC_ZX:

    case DEC_A:

    case CMP_IdX:

    case ASL_ZX:

    case ASL_A:

    case AND_IdX:

    case ORA_IdX:

    case ADC_IdX:

    case STA_IdX:

    case STA_IdY:

    case SBC_IdX:
        cycles = 6;
        break;

    case LDA_IdY:

    case EOR_IdY:

    case AND_IdY:

    case CMP_IdY:

    case ORA_IdY:

    case SBC_IdY:

    case ADC_IdY:
        if (is_page_crossed(indirect_indexed(cpu, cpu->program_counter + 1, 0), cpu->register_y)) {
            cycles = 6;
        } else {
            cycles = 5;
        }
        break;

    case ROL_AX:

    case ROR_AX:

    case LSR_AX:

    case INC_AX:

    case DEC_AX:

    case BRK:

    case ASL_AX:
        cycles = 7;
        break;

    case BCC:
        if (!GET_C_FLAG(cpu)) {
            cycles = 1;
        }

    branch_page_test:
        if (is_page_crossed(cpu->program_counter, cpu->mem[cpu->program_counter + 1])) {
            cycles += 4;
        } else {
            cycles += 2;
        }

        break;

    case BEQ:
        if (GET_Z_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    case BCS:
        if (GET_C_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    case BMI:
        if (GET_N_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    case BNE:
        if (!GET_Z_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    case BPL:
        if (!GET_N_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    case BVC:
        if (!GET_V_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    case BVS:
        if (GET_V_FLAG(cpu)) {
            cycles = 1;
        }
        goto branch_page_test;
        break;

    default:
        cycles = 0;
    }

    return cycles;
}

errcode_t execute_instruction(CPU* cpu) {

    switch (cpu->mem[cpu->program_counter]) {

    case ADC_I:
        ADD_immediate(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ADC_Z:
        ADD_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ADC_ZX:
        ADD_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case ADC_A:
        ADD_absolute(cpu, get_address(cpu));
        break;

    case ADC_AX:
        ADD_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case ADC_AY:
        ADD_offset(cpu, get_address(cpu), cpu->register_y);
        break;

    case ADC_IdX:
        ADD_indexed_indirect(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case ADC_IdY:
        ADD_indirect_indexed(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_y);
        break;

    case AND_I:
        AND_immediate(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case AND_Z:
        AND_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case AND_ZX:
        AND_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case AND_A:
        AND_absolute(cpu, get_address(cpu));
        break;

    case AND_AX:
        AND_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case AND_AY:
        AND_offset(cpu, get_address(cpu), cpu->register_y);
        break;

    case AND_IdX:
        AND_indexed_indirect(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case AND_IdY:
        AND_indirect_indexed(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_y);
        break;

    case ASL_Ac:
        SHIFT_LEFT_ACC(cpu);
        break;

    case ASL_Z:
        SHIFT_LEFT_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ASL_ZX:
        SHIFT_LEFT_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case ASL_A:
        SHIFT_LEFT_absolute(cpu, get_address(cpu));
        break;

    case ASL_AX:
        SHIFT_LEFT_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case BCC:
        BRANCH_Carry_C(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BCS:
        BRANCH_Carry_S(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BEQ:
        BRANCH_Equal(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BIT_Z:
        BIT_TEST(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BIT_A:
        BIT_TEST(cpu, get_address(cpu));
        break;

    case BMI:
        BRANCH_If_Minus(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BNE:
        BRANCH_Not_Equal(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BPL:
        BRANCH_If_Positive(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BRK:
        BREAK(cpu);
        break;

    case BVC:
        BRANCH_Overflow_C(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case BVS:
        BRANCH_Overflow_S(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CLC:
        CLEAR_C_FLAG(cpu);
        break;

    case CLD:
        CLEAR_D_FLAG(cpu);
        break;

    case CLI:
        CLEAR_I_FLAG(cpu);
        break;

    case CLV:
        CLEAR_V_FLAG(cpu);
        break;

    case CMP_I:
        COMPARE_Immediate_ACC(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CMP_A:
        COMPARE_absolute_ACC(cpu, get_address(cpu));
        break;

    case CMP_AX:
        COMPARE_offset_ACC(cpu, get_address(cpu), cpu->register_x);
        break;

    case CMP_AY:
        COMPARE_offset_ACC(cpu, get_address(cpu), cpu->register_y);
        break;

    case CMP_IdX:
        COMPARE_indexed_indirect_ACC(cpu, get_address(cpu), cpu->register_x);
        break;

    case CMP_IdY:
        COMPARE_indirect_indexed_ACC(cpu, get_address(cpu), cpu->register_y);
        break;

    case CMP_Z:
        COMPARE_absolute_ACC(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CMP_ZX:
        COMPARE_offset_ACC(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case CPX_I:
        COMPARE_Immediate_X(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CPX_Z:
        COMPARE_absolute_X(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CPX_A:
        COMPARE_absolute_X(cpu, get_address(cpu));
        break;

    case CPY_I:
        COMPARE_Immediate_Y(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CPY_Z:
        COMPARE_absolute_Y(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case CPY_A:
        COMPARE_absolute_Y(cpu, get_address(cpu));
        break;

    case DEX:
        DEC_X(cpu);
        break;

    case DEY:
        DEC_Y(cpu);
        break;

    case DEC_Z:
        DEC_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case DEC_A:
        DEC_absolute(cpu, get_address(cpu));
        break;

    case DEC_ZX:
        DEC_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case DEC_AX:
        DEC_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case INC_Z:
        INC_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case INC_A:
        INC_absolute(cpu, get_address(cpu));
        break;

    case INC_ZX:
        INC_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case INC_AX:
        INC_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case INX:
        INC_X(cpu);
        break;

    case INY:
        INC_Y(cpu);
        break;

    case JMP_A:
        JUMP_absolute(cpu, get_address(cpu));
        break;

    case JMP_Id:
        JUMP_indirect(cpu, get_address(cpu));
        break;

    case EOR_I:
        XOR_immediate(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case EOR_Z:
        XOR_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case EOR_ZX:
        XOR_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case EOR_A:
        XOR_absolute(cpu, get_address(cpu));
        break;

    case EOR_AX:
        XOR_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case EOR_AY:
        XOR_offset(cpu, get_address(cpu), cpu->register_y);
        break;

    case EOR_IdX:
        XOR_indexed_indirect(cpu, get_address(cpu), cpu->register_x);
        break;

    case EOR_IdY:
        XOR_indirect_indexed(cpu, get_address(cpu), cpu->register_y);
        break;

    case JSR:
        JUMP_Subroutine(cpu, get_address(cpu));
        break;

    case LDA_I:
        LD_immediate_ACC(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LDA_Z:
        LD_absolute_ACC(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LDA_ZX:
        LD_offset_ACC(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case LDA_A:
        LD_absolute_ACC(cpu, get_address(cpu));
        break;

    case LDA_AX:
        LD_offset_ACC(cpu, get_address(cpu), cpu->register_x);
        break;

    case LDA_AY:
        LD_offset_ACC(cpu, get_address(cpu), cpu->register_y);
        break;

    case LDA_IdX:
        LD_indexed_indirect_ACC(cpu, get_address(cpu), cpu->register_x);
        break;

    case LDA_IdY:
        LD_indirect_indexed_ACC(cpu, get_address(cpu), cpu->register_y);
        break;

    case LDX_I:
        LD_immediate_X(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LDX_Z:
        LD_absolute_X(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LDX_ZY:
        LD_offset_X(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_y);
        break;

    case LDX_A:
        LD_absolute_X(cpu, get_address(cpu));
        break;

    case LDX_AY:
        LD_offset_X(cpu, get_address(cpu), cpu->register_y);
        break;

    case LDY_I:
        LD_immediate_Y(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LDY_Z:
        LD_absolute_Y(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LDY_ZX:
        LD_offset_Y(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_y);
        break;

    case LDY_A:
        LD_absolute_Y(cpu, get_address(cpu));
        break;

    case LDY_AX:
        LD_offset_Y(cpu, get_address(cpu), cpu->register_x);
        break;

    case LSR_Ac:
        SHIFT_RIGHT_ACC(cpu);
        break;

    case LSR_Z:
        SHIFT_RIGHT_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case LSR_ZX:
        SHIFT_RIGHT_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case LSR_A:
        SHIFT_RIGHT_absolute(cpu, get_address(cpu));
        break;

    case LSR_AX:
        SHIFT_RIGHT_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case NOP:
        break;

    case ORA_I:
        OR_immediate(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ORA_Z:
        OR_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ORA_ZX:
        OR_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case ORA_A:
        OR_absolute(cpu, get_address(cpu));
        break;

    case ORA_AX:
        OR_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case ORA_AY:
        OR_offset(cpu, get_address(cpu), cpu->register_y);
        break;

    case ORA_IdX:
        OR_indexed_indirect(cpu, get_address(cpu), cpu->register_x);
        break;

    case ORA_IdY:
        OR_indirect_indexed(cpu, get_address(cpu), cpu->register_y);
        break;

    case PHA:
        PUSH_ACC(cpu);
        break;

    case PHP:
        PUSH_FLAGS(cpu);
        break;

    case PLA:
        PULL_ACC(cpu);
        break;

    case PLP:
        PULL_FLAGS(cpu);
        break;

    case ROL_Ac:
        ROTATION_LEFT_ACC(cpu);
        break;

    case ROL_Z:
        ROTATION_LEFT_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ROL_ZX:
        ROTATION_LEFT_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case ROL_A:
        ROTATION_LEFT_absolute(cpu, get_address(cpu));
        break;

    case ROL_AX:
        ROTATION_LEFT_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case ROR_Ac:
        ROTATION_RIGHT_ACC(cpu);
        break;

    case ROR_Z:
        ROTATION_RIGHT_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case ROR_ZX:
        ROTATION_RIGHT_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case ROR_A:
        ROTATION_RIGHT_absolute(cpu, get_address(cpu));
        break;

    case ROR_AX:
        ROTATION_RIGHT_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case RTI:
        RETURN_Interrupt(cpu);
        break;

    case RTS:
        RETURN_Subroutine(cpu);
        break;

    case SBC_I:
        SBC_immediate(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case SBC_Z:
        SBC_absolute(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case SBC_ZX:
        SBC_offset(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case SBC_A:
        SBC_absolute(cpu, get_address(cpu));
        break;

    case SBC_AX:
        SBC_offset(cpu, get_address(cpu), cpu->register_x);
        break;

    case SBC_AY:
        SBC_offset(cpu, get_address(cpu), cpu->register_y);
        break;

    case SBC_IdX:
        SBC_indexed_indirect(cpu, get_address(cpu), cpu->register_x);
        break;

    case SBC_IdY:
        SBC_indirect_indexed(cpu, get_address(cpu), cpu->register_y);
        break;

    case SEC:
        SET_C_FLAG(cpu);
        break;

    case SED:
        SET_D_FLAG(cpu);
        break;

    case SEI:
        SET_I_FLAG(cpu);
        break;

    case STA_Z:
        STR_absolute_ACC(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case STA_ZX:
        STR_offset_ACC(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case STA_A:
        STR_absolute_ACC(cpu, get_address(cpu));
        break;

    case STA_AX:
        STR_offset_ACC(cpu, get_address(cpu), cpu->register_x);
        break;

    case STA_AY:
        STR_offset_ACC(cpu, get_address(cpu), cpu->register_y);
        break;

    case STA_IdX:
        STR_indexed_indirect_ACC(cpu, get_address(cpu), cpu->register_x);
        break;

    case STA_IdY:
        STR_indirect_indexed_ACC(cpu, get_address(cpu), cpu->register_y);
        break;

    case STX_Z:
        STR_absolute_X(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case STX_ZY:
        STR_offset_X(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_y);
        break;

    case STX_A:
        STR_absolute_X(cpu, get_address(cpu));
        break;

    case STY_Z:
        STR_absolute_Y(cpu, cpu->mem[cpu->program_counter + 1]);
        break;

    case STY_ZX:
        STR_offset_Y(cpu, cpu->mem[cpu->program_counter + 1], cpu->register_x);
        break;

    case STY_A:
        STR_absolute_Y(cpu, get_address(cpu));
        break;

    case TAX:
        TR_ACC_X(cpu);
        break;

    case TSX:
        TR_SP_X(cpu);
        break;

    case TXA:
        TR_X_ACC(cpu);
        break;

    case TYA:
        TR_Y_ACC(cpu);
        break;

    case TAY:
        TR_ACC_Y(cpu);
        break;

    case TXS:
        TR_X_SP(cpu);
        break;

    default:
        return ERR_ABN_OPCODE;
        break;
    }

    return 0;
}

errcode_t step_cpu(CPU* cpu) {
    if (cpu->cycle_counter == 0) {
        if (execute_instruction(cpu)) {
            goto error;
        };
        cpu->cycle_counter = get_next_instruction(cpu);

        if (cpu->cycle_counter == 0) {
        error:
            printf("Abnormal opcode: %#02x\n", cpu->mem[cpu->program_counter]);
            return ERR_ABN_OPCODE;
        }
        memory_mirroring(cpu->mem);
        return 0;
    }

    --(cpu->cycle_counter);
    return 0;
}

void print_cpu_state(CPU* processor) {

    printf("|======== Registers State ========|\n");
    printf("Accumulator: %i\n", processor->accumulator);
    printf("X Register: %i\n", processor->register_x);
    printf("Y Register: %i\n", processor->register_y);
    printf("Program Counter: %#04x\n", processor->program_counter);
    printf("Stack Pointer: %#02x\n", processor->stack_pointer);
    printf("Flags: %#02x\n", processor->flags);

    printf("\n");

    printf("|======== Flags State ========|\n");
    printf("Negative Flag (N): %i\n", (GET_N_FLAG(processor) >> 7));
    printf("Overflow Flag (V): %i\n", (GET_V_FLAG(processor) >> 6));
    printf("Pause Flag (P): %i\n", (GET_P_FLAG(processor) >> 5));
    printf("Break Flag (B): %i\n", (GET_B_FLAG(processor) >> 4));
    printf("Decimal Flag (D): %i\n", (GET_D_FLAG(processor) >> 3));
    printf("Interrupt Flag (I): %i\n", (GET_I_FLAG(processor) >> 2));
    printf("Zero Flag (Z): %i\n", (GET_Z_FLAG(processor) >> 1));
    printf("Carry Flag (C): %i\n", GET_C_FLAG(processor));

    printf("\n");
}

void print_cpu_expected(data acc, data x, data y, data flags, address pc, data sp) {
    printf("|======== EXPECTED: Registers State ========|\n");
    printf("Accumulator: %i\n", acc);
    printf("X Register: %i\n", x);
    printf("Y Register: %i\n", y);
    printf("Program Counter: %#04x\n", pc);
    printf("Stack Pointer: %#02x\n", sp);
    printf("Flags: %#02x\n", flags);

    printf("\n");

    printf("|======== EXPECTED: Flags State ========|\n");
    printf("Negative Flag (N): %i\n", ((flags & BIT_7_MASK) >> 7));
    printf("Overflow Flag (V): %i\n", ((flags & BIT_6_MASK) >> 6));
    printf("Pause Flag (P): %i\n", ((flags & BIT_5_MASK) >> 5));
    printf("Break Flag (B): %i\n", ((flags & BIT_4_MASK) >> 4));
    printf("Decimal Flag (D): %i\n", ((flags & BIT_3_MASK) >> 3));
    printf("Interrupt Flag (I): %i\n", ((flags & BIT_2_MASK) >> 2));
    printf("Zero Flag (Z): %i\n", ((flags & BIT_1_MASK) >> 1));
    printf("Carry Flag (C): %i\n", (flags & BIT_0_MASK));

    printf("\n");
}

void print_stack_state(CPU* cpu) {

    printf("<====== Stack State =====>\n");
    printf("Stack pointer: %#02x\n", cpu->stack_pointer);

    if (cpu->stack_pointer == 0xFF) {
        printf("\n");
        return;
    }

    printf("sp:  | int |\t | hex |\n\n");

    for (uint8_t sp = cpu->stack_pointer + 1; sp < 0xFF; ++sp) {
        printf("%#02x: | %i |", sp, cpu->mem[STACK_END | sp]);
        printf("\t | %#02x |\n", cpu->mem[STACK_END | sp]);
    }

    printf("%#02x: | %i |", 0xFF, cpu->mem[STACK_START]);
    printf("\t | %#02x |\n", cpu->mem[STACK_START]);

    printf("\n");
}

void print_stack_expected(memory stack, data start) {
    printf("<===== EXPECTED: Stack State =====>\n");
    printf("Stack pointer: %#02x\n", start);

    if (start == 0xFF) {
        printf("\n");
        return;
    }

    printf("sp:  | int |\t | hex |\n\n");

    for (data sp = start + 1; sp < 0xFF; ++sp) {
        printf("%#02x: | %i |", sp, stack[sp]);
        printf("\t | %#02x |\n", stack[sp]);
    }

    printf("%#02x: | %i |", 0xFF, stack[0xFF]);
    printf("\t | %#02x |\n", stack[0xFF]);

    printf("\n");
}

// The routine to implement when implementing hte reset
/*
Reset:
        sei ; disables all interr:upts of the NES
        cld ; disable decimal mode, the NES doesn't actually support the decimal mode when doing
calculation


        ; disable the IRQ (Interrupt Request) from the APU
        ; basically, we're preventing strange sound to play on startup
        ldx #$40
        stx APU_FRAMEC


        ; Initialize the Stack register
        ldx #$FF  ; the Stack pointer offset starts at the last address possible
        txs       ; we're transfering the offset to the correct register

        inx 	  ; we're doing that to reset the value in the X register. In a 8bit architecture,
doing #$FF + 1 = #$00


        ; Zero out the PPU registers

        stx PPU_CTRL
        stx PPU_MASK


        ; Zero out the DMC_CTRL Register
        ; Prevent weird sounds from appearing randomly on startup

        stx DMC_CTRL


        ; We have to wait for the first vBlank. Which means that the PPU has correctly start

:
        bit PPU_STATUS ; bit op code tells us is the signed bit is set to 1 or 0. | If set to 1,
then this is a negative bit, so BPL (Branch if PLus/Positive) will not branch. bpl :-         ;
branch back to the previous anonymous label if the PPU is not drawing a vBlank.

        txa	       ; transfer x value to the acc. Here: Zero out the Acc
*/
