#include "core/mem.h"
#include "macros.h"

#include <stdlib.h>
#include <sys/mman.h>

memory init_memory() {

    memory mem =
        mmap(NULL, TOTAL_MEM_SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

    if (mem == MAP_FAILED) {
        exit(EXIT_FAILURE);
    }

    return mem;
}

void reset_memory(memory mem) {
    for (unsigned int i = 0; i < TOTAL_MEM_SIZE; ++i) {
        mem[i] = 0x00;
    }
}

void free_memory(memory mem) {
    munmap(mem, TOTAL_MEM_SIZE);
}

void memory_mirroring(memory mem) {
    for (unsigned int i = 0; i < 0x800; ++i) {
        mem[RAM_MIRROR_1_START + i] = mem[i];
        mem[RAM_MIRROR_2_START + i] = mem[i];
        mem[RAM_MIRROR_3_START + i] = mem[i];
    }

    for (unsigned int i = 0; i < 0x1ff8; i += 8) {
        mem[PPU_MIRRORS_START_ZERO + i] = mem[PPU_REG_ZERO];
        mem[PPU_MIRRORS_START_ONE + i] = mem[PPU_REG_ONE];
        mem[PPU_MIRRORS_START_TWO + i] = mem[PPU_REG_TWO];
        mem[PPU_MIRRORS_START_THREE + i] = mem[PPU_REG_THREE];
        mem[PPU_MIRRORS_START_FOUR + i] = mem[PPU_REG_FOUR];
        mem[PPU_MIRRORS_START_FIVE + i] = mem[PPU_REG_FIVE];
        mem[PPU_MIRRORS_START_SIX + i] = mem[PPU_REG_SIX];
        mem[PPU_MIRRORS_START_SEVEN + i] = mem[PPU_REG_SEVEN];
    }
}
