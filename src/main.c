#include "UI/tui.h"
#include "core/cpu.h"
#include "core/mem.h"
#include "macros.h"
#include "rom_reader/reader.h"

#include "UI/ui.h"
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern int opterr;

int main(int argc, char** argv) {

    int opt;
    rom* my_rom = NULL;

    enum ui_mode mode = OTHER;
    bool debug = FALSE;
    opterr = 0;
    const struct option options[] = {{"opt-arg", optional_argument, 0, 't'},
                                     {"req-arg", required_argument, 0, 'i'},
                                     {"no-arg", no_argument, 0, 'd'},
                                     {NULL, 0, 0, '\0'}};

    while ((opt = getopt_long(argc, argv, "di:t::", options, NULL)) != -1) {

        switch (opt) {
        case 'i':
            if (optarg == NULL) {
                goto err;
            }
            printf("Filepath: %s\n", optarg);
            my_rom = open_rom(optarg);
            break;
        case 't':
            // en_US.UTF-8
            if (optarg == NULL) {
                init_nscreen("en_US.UTF-8");
            } else {
                init_nscreen(optarg);
            }
            mode = TUI;
            break;
        case 'd':
            debug = TRUE;
            break;
        default:
        err:
            fprintf(stderr, "Usage: %s [-t[locale]] [-i path/to/rom]\n", argv[0]);
            break;
        }
    }

    unsigned char ch = 0;

    CPU* cpu = (CPU*)malloc(sizeof(CPU));
    memory mem = init_memory();

    init_cpu(cpu, mem);
    reset_memory(cpu->mem);

    // cpu->flags = 0xff;

    if (debug) {
        display_rom(my_rom);
    }

    if (mode == TUI) {
        while (ch != (char)KEY_F(1)) {
            printw("\nPress 's' to go to the next step.\n");
            printw_cpu_state(cpu);

            ch = getch();
            erase();

            if (ch == (char)KEY_F(1)) {
                printw("Press any key to leave");
            } else if (ch == 's') {
                printw("New step !\n");
                INC_X(cpu);
            }
        }
        end_nscreen();
    }

    free_cpu(cpu);

    if (my_rom != NULL) {
        free_rom(my_rom);
    }

    return EXIT_SUCCESS;
}
