#include "rom_reader/reader.h"
#include "macros.h"

#include <stdio.h>
#include <stdlib.h>

rom* open_rom(char* filepath) {
    FILE* file;
    size_t fsize;

    file = fopen(filepath, "rb");
    if (file == NULL) {
        perror("Error opening the file !\n");
        exit(EXIT_FAILURE);
    }

    fseek(file, 0, SEEK_END);
    fsize = ftell(file);
    fseek(file, 0, SEEK_SET);

    rom_buffer buff = (rom_buffer)malloc(fsize);
    if (!buff) {
        perror("Memory allocation failed !\n");
        fclose(file);
        exit(EXIT_FAILURE);
    }

    size_t result = fread(buff, 1, fsize, file);
    printf("\nRead: %lu\n", result);
    if (result != fsize) {
        perror("Reading error !\n");
        free(buff);
        fclose(file);
        exit(EXIT_FAILURE);
    }

    fclose(file);

    unsigned int file_id = (buff[0] << 24) | (buff[1] << 16) | (buff[2] << 8) | (buff[3]);
    if (file_id != NES_SIGNATURE) {
        printf("Signature is not matching ! %i != %i\n", file_id, NES_SIGNATURE);
        printf("Only NES ROMs are accepted by this emulator !\n\n");
        free(buff);
        exit(EXIT_FAILURE);
    }

    rom* r = (rom*)malloc(sizeof(rom));
    if (r == NULL) {
        perror("Allocation failure !\n");
        free(buff);
        exit(EXIT_FAILURE);
    }
    r->buff = buff;
    r->buff_size = fsize;

    return r;
}

void display_rom(rom* r) {
    if (r == NULL) {
        printf("No rom found. \n");
        return;
    }
    int char_displayed = 0;
    for (size_t i = 0; i < r->buff_size; ++i) {
        printf("%02x ", r->buff[i]);
        ++char_displayed;
        if (char_displayed == 16) {
            printf("\n");
            char_displayed = 0;
        }
    }
}

void free_rom(rom* r) {
    free(r->buff);
    free(r);
}
