#include "core/cpu.h"
#include "core/mem.h"
#include "macros.h"
#include "rom_reader/reader.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void print_cpu_state(CPU* processor) {

    printf("|======== Registers State ========|\n");
    printf("Accumulator: %i\n", processor->accumulator);
    printf("X Register: %i\n", processor->register_x);
    printf("Y Register: %i\n", processor->register_y);
    printf("Program Counter: %#04x\n", processor->program_counter);
    printf("Stack Pointer: %#02x\n", processor->stack_pointer);
    printf("Flags: %#02x\n", processor->flags);

    printf("\n");

    printf("|======== Flags State ========|\n");
    printf("Negative Flag (N): %i\n", (GET_N_FLAG(processor) >> 7));
    printf("Overflow Flag (V): %i\n", (GET_V_FLAG(processor) >> 6));
    printf("Pause Flag (P): %i\n", (GET_P_FLAG(processor) >> 5));
    printf("Break Flag (B): %i\n", (GET_B_FLAG(processor) >> 4));
    printf("Decimal Flag (D): %i\n", (GET_D_FLAG(processor) >> 3));
    printf("Interrupt Flag (I): %i\n", (GET_I_FLAG(processor) >> 2));
    printf("Zero Flag (Z): %i\n", (GET_Z_FLAG(processor) >> 1));
    printf("Carry Flag (C): %i\n", GET_C_FLAG(processor));

    printf("\n");
}

void print_cpu_expected(data acc, data x, data y, data flags, address pc, data sp) {
    printf("|======== EXPECTED: Registers State ========|\n");
    printf("Accumulator: %i\n", acc);
    printf("X Register: %i\n", x);
    printf("Y Register: %i\n", y);
    printf("Program Counter: %#04x\n", pc);
    printf("Stack Pointer: %#02x\n", sp);
    printf("Flags: %#02x\n", flags);

    printf("\n");

    printf("|======== EXPECTED: Flags State ========|\n");
    printf("Negative Flag (N): %i\n", ((flags & BIT_7_MASK) >> 7));
    printf("Overflow Flag (V): %i\n", ((flags & BIT_6_MASK) >> 6));
    printf("Pause Flag (P): %i\n", ((flags & BIT_5_MASK) >> 5));
    printf("Break Flag (B): %i\n", ((flags & BIT_4_MASK) >> 4));
    printf("Decimal Flag (D): %i\n", ((flags & BIT_3_MASK) >> 3));
    printf("Interrupt Flag (I): %i\n", ((flags & BIT_2_MASK) >> 2));
    printf("Zero Flag (Z): %i\n", ((flags & BIT_1_MASK) >> 1));
    printf("Carry Flag (C): %i\n", (flags & BIT_0_MASK));

    printf("\n");
}

void print_stack_state(CPU* cpu) {

    printf("<====== Stack State =====>\n");
    printf("Stack pointer: %#02x\n", cpu->stack_pointer);

    if (cpu->stack_pointer == 0xFF) {
        printf("\n");
        return;
    }

    printf("sp:  | int |\t | hex |\n\n");

    for (uint8_t sp = cpu->stack_pointer + 1; sp < 0xFF; ++sp) {
        printf("%#02x: | %i |", sp, cpu->mem[STACK_END | sp]);
        printf("\t | %#02x |\n", cpu->mem[STACK_END | sp]);
    }

    printf("%#02x: | %i |", 0xFF, cpu->mem[STACK_START]);
    printf("\t | %#02x |\n", cpu->mem[STACK_START]);

    printf("\n");
}

void print_stack_expected(memory stack, data start) {
    printf("<===== EXPECTED: Stack State =====>\n");
    printf("Stack pointer: %#02x\n", start);

    if (start == 0xFF) {
        printf("\n");
        return;
    }

    printf("sp:  | int |\t | hex |\n\n");

    for (data sp = start + 1; sp < 0xFF; ++sp) {
        printf("%#02x: | %i |", sp, stack[sp]);
        printf("\t | %#02x |\n", stack[sp]);
    }

    printf("%#02x: | %i |", 0xFF, stack[0xFF]);
    printf("\t | %#02x |\n", stack[0xFF]);

    printf("\n");
}

int main(int argc, char** argv) {

    int opt;
    rom* my_rom = NULL;

    while ((opt = getopt(argc, argv, ":i:")) != -1) {
        switch (opt) {
        case 'i':
            printf("Filepath: %s\n", optarg);
            my_rom = open_rom(optarg);
            if (my_rom != NULL) {
                display_rom(my_rom);
                free_rom(my_rom);
            }
            return EXIT_SUCCESS;
            break;
        default:
            break;
        }
    }

    CPU* cpu = (CPU*)malloc(sizeof(CPU));
    data stack_test[256];

    memory mem = init_memory();
    init_cpu(cpu, mem);
    reset_memory(cpu->mem);

    for (unsigned int i = 0; i < 50; ++i) {
        cpu->mem[i] = i;
    }

    printf("##### Load Tests #####\n\n");

    LD_immediate_ACC(cpu, 5);
    LD_immediate_X(cpu, 5);
    LD_immediate_Y(cpu, 5);

    print_cpu_expected(5, 5, 5, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    LD_offset_ACC(cpu, 0, 0);
    LD_offset_X(cpu, 0, 1);
    LD_offset_Y(cpu, 0, 2);

    print_cpu_expected(0, 1, 2, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    LD_absolute_ACC(cpu, 3);
    LD_absolute_X(cpu, 4);
    LD_absolute_Y(cpu, 5);

    print_cpu_expected(3, 4, 5, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    cpu->mem[0 | 1 << 8] = 121;
    cpu->mem[1 | 2 << 8] = 122;
    cpu->mem[2 | 3 << 8] = 123;

    LD_indirect_ACC(cpu, 0); // Expected: 121
    LD_indirect_X(cpu, 1);   // Expected: 122
    LD_indirect_Y(cpu, 2);   // Expected: 123

    print_cpu_expected(121, 122, 123, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    LD_immediate_X(cpu, 0);
    LD_immediate_Y(cpu, 0);

    printf("##### INC/DEC Tests #####\n\n");

    INC_X(cpu);
    INC_Y(cpu);

    print_cpu_expected(121, 1, 1, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    DEC_X(cpu);
    DEC_Y(cpu);

    print_cpu_expected(121, 0, 0, 0b00000010, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    printf("##### Indirections test #####\n\n");

    cpu->mem[(5) | (6) << 8] = 150;

    LD_immediate_X(cpu, 5);
    LD_indexed_indirect_ACC(cpu, 0, cpu->register_x);

    print_cpu_expected(150, 5, 0, 0b10000000, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    cpu->mem[0] = 1;
    cpu->mem[1] = 1;

    cpu->mem[((1) | (1) << 8) + 2] = 5;

    LD_immediate_Y(cpu, 2);

    LD_indirect_indexed_ACC(cpu, 0, cpu->register_y);

    print_cpu_expected(5, 5, 2, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    stack_test[0xFF] = 5;

    printf("##### PUSH/PULL tests #####\n\n");

    LD_immediate_ACC(cpu, 5);
    PUSH_ACC(cpu);

    print_stack_expected(stack_test, 0xFE);
    print_stack_state(cpu);

    LD_immediate_ACC(cpu, 0);
    LD_immediate_X(cpu, 0);
    LD_immediate_Y(cpu, 0);

    PULL_ACC(cpu);

    print_cpu_expected(5, 0, 0, 0x00, 0xFFFC, 0xFF);
    print_cpu_state(cpu);

    print_stack_expected(stack_test, 0xFF);
    print_stack_state(cpu);

    PUSH_PC(cpu);

    stack_test[0xFF] = 0xFC;
    stack_test[0xFE] = 0xFF;

    print_stack_expected(stack_test, 0xFD);
    print_stack_state(cpu);

    stack_test[0xFF] = 0x0C;
    stack_test[0xFE] = 0x81;

    cpu->mem[STACK_END | 0xFF] = 0x0C;
    cpu->mem[STACK_END | 0xFE] = 0x81;

    print_stack_expected(stack_test, 0xFD);
    print_stack_state(cpu);

    PULL_PC(cpu);

    print_cpu_expected(5, 0, 0, 0x00, 0x810C, 0xFF);
    print_cpu_state(cpu);

    cpu->flags = 0x9c;

    PUSH_FLAGS(cpu);

    stack_test[0xFF] = 0x9c;

    print_cpu_expected(5, 0, 0, 0x9c, 0x810C, 0xFE);
    print_cpu_state(cpu);

    print_stack_expected(stack_test, 0xFE);
    print_stack_state(cpu);

    CLEAR_ALL_FLAGS(cpu);

    cpu->mem[STACK_END | 0xFF] = 0x69;

    PULL_FLAGS(cpu);

    print_stack_expected(stack_test, 0xFF);
    print_stack_state(cpu);

    print_cpu_expected(5, 0, 0, 0x69, 0x810C, 0xFF);
    print_cpu_state(cpu);

    printf("##### ADD Tests #####\n\n");

    CLEAR_ALL_FLAGS(cpu);

    LD_immediate_ACC(cpu, 0);
    ADD_immediate(cpu, 1);

    print_cpu_expected(1, 0, 0, 0x00, 0x810C, 0xFF);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 0);
    ADD_immediate(cpu, 0);

    print_cpu_expected(0, 0, 0, 0b00000010, 0x810C, 0xFF);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 255);
    ADD_immediate(cpu, 1);

    print_cpu_expected(0, 0, 0, 0b01000011, 0x810C, 0xFF);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 0);
    cpu->mem[20] = 5;
    ADD_offset(cpu, 15, 5);

    print_cpu_expected(5, 0, 0, 0x00, 0x810C, 0xFF);
    print_cpu_state(cpu);

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 0);

    cpu->mem[0] = 0x11;
    cpu->mem[1] = 0x11;
    cpu->mem[0x1111] = 20;

    ADD_indirect_indexed(cpu, 0, 0);

    print_cpu_expected(20, 0, 0, 0x00, 0x810C, 0xFF);
    print_cpu_state(cpu);

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 0);

    ADD_indexed_indirect(cpu, 0, 0);

    print_cpu_expected(20, 0, 0, 0x00, 0x810C, 0xFF);
    print_cpu_state(cpu);

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 255);

    ADD_immediate(cpu, 1);

    print_cpu_expected(0, 0, 0, 0b01000010, 0x810c, 0xff);
    print_cpu_state(cpu);

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 0b11001001);

    printf("##### And Tests #####\n\n");

    AND_immediate(cpu, 0b00000001);

    print_cpu_expected(1, 0, 0, 0x00, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0x5040] = 0xf1;

    AND_absolute(cpu, 0x5040);

    print_cpu_expected(1, 0, 0, 0x00, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 0b10000001);
    cpu->mem[1] = 0xff;

    AND_offset(cpu, 0x00, 1);

    print_cpu_expected(0b10000001, 0, 0, BIT_7_MASK, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0] = 0x11;
    cpu->mem[1] = 0x11;
    cpu->mem[0x1111] = 0x00;

    AND_indirect_indexed(cpu, 0x00, 0);

    print_cpu_expected(0, 0, 0, BIT_1_MASK, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 128);

    AND_indexed_indirect(cpu, 0, 0);

    print_cpu_expected(0, 0, 0, 0b000000010, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### Or Tests #####\n\n");

    OR_immediate(cpu, 0b00000001);

    print_cpu_expected(0b00000001, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0] = 0b00000010;

    OR_absolute(cpu, 0);

    print_cpu_expected(0b00000011, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[1] = 0b00000100;

    OR_offset(cpu, 0, 1);

    print_cpu_expected(0b00000111, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[2] = 0x11;
    cpu->mem[3] = 0x11;
    cpu->mem[0x1111] = 0b00001000;

    OR_indirect_indexed(cpu, 2, 0);

    print_cpu_expected(0b00001111, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0x1111] = 0b00010000;

    OR_indexed_indirect(cpu, 2, 0);

    print_cpu_expected(0b00011111, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### Xor Tests #####\n\n");

    LD_immediate_ACC(cpu, 0b11111100);
    XOR_immediate(cpu, 0b00111111);

    print_cpu_expected(0b11000011, 0, 0, 0b10000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0] = 0b11000011;

    XOR_absolute(cpu, 0);

    print_cpu_expected(0, 0, 0, 0b00000010, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[1] = 0b10101010;
    XOR_offset(cpu, 0, 1);

    print_cpu_expected(0b10101010, 0, 0, 0b10000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0] = 0x55;
    cpu->mem[1] = 0x55;

    cpu->mem[0x5555] = 0b01010101;

    XOR_indirect_indexed(cpu, 0, 0);

    print_cpu_expected(0xff, 0, 0, 0b10000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0x5555] = 0xff;

    XOR_indexed_indirect(cpu, 0, 0);

    print_cpu_expected(0, 0, 0, 2, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### Sbc Tests #####\n\n");

    SBC_immediate(cpu, 1);

    print_cpu_expected(0xff, 0, 0, 0b11000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 80);

    cpu->mem[0] = 0x01;

    SBC_absolute(cpu, 0x00);

    print_cpu_expected(79, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[1] = 0x02;

    SBC_offset(cpu, 0, 1);

    print_cpu_expected(77, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0] = 0x11;
    cpu->mem[1] = 0x11;

    cpu->mem[0x1111] = 10;

    SBC_indirect_indexed(cpu, 0, 0);

    print_cpu_expected(67, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    SBC_indexed_indirect(cpu, 0, 0);

    print_cpu_expected(57, 0, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### TR Tests #####\n\n");

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 10);
    TR_ACC_X(cpu);

    print_cpu_expected(10, 10, 0, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    TR_ACC_Y(cpu);

    print_cpu_expected(10, 10, 10, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_X(cpu, 50);
    TR_X_ACC(cpu);

    print_cpu_expected(50, 50, 10, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    TR_Y_ACC(cpu);

    print_cpu_expected(10, 50, 10, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    TR_SP_X(cpu);

    print_cpu_expected(10, 0xff, 10, 0b10000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_X(cpu, 0xfe);
    TR_X_SP(cpu);

    print_cpu_expected(10, 0xfe, 10, 0b10000000, 0x810c, 0xfe);
    print_cpu_state(cpu);

    cpu->stack_pointer = 0xff;
    cpu->register_x = 10;
    CLEAR_ALL_FLAGS(cpu);

    printf("##### STR Tests #####\n\n");

    STR_absolute_ACC(cpu, 0x1100);
    printf("\nExpected: 10 | %i\n", cpu->mem[0x1100]);

    STR_absolute_X(cpu, 0x1101);
    printf("\nExpected: 10 | %i\n", cpu->mem[0x1101]);

    STR_absolute_Y(cpu, 0x1102);
    printf("\nExpected: 10 | %i\n", cpu->mem[0x1102]);

    LD_immediate_ACC(cpu, 15);
    LD_immediate_X(cpu, 16);
    LD_immediate_Y(cpu, 17);

    STR_offset_ACC(cpu, 0x00, 5);
    printf("\nExpected: 15 | %i\n", cpu->mem[0x05]);

    STR_offset_X(cpu, 0x00, 6);
    printf("\nExpected: 16 | %i\n", cpu->mem[0x06]);

    STR_offset_Y(cpu, 0x00, 7);
    printf("\nExpected: 17 | %i\n", cpu->mem[0x07]);

    STR_indirect_indexed_ACC(cpu, 0x10, 5);
    printf("\nExpected: 15 | %i\n", indirect_indexed(cpu, 0x10, 5));

    STR_indexed_indirect_ACC(cpu, 0x10, 5);
    printf("\nExpected: 15 | %i\n", indexed_indirect(cpu, 0x10, 5));

    printf("##### BIT Tests #####\n\n");

    cpu->mem[0x00] = 0xFF;
    cpu->mem[0x01] = 0b00110011;
    cpu->mem[0x02] = 0x00;
    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 15);
    LD_immediate_X(cpu, 1);
    LD_immediate_Y(cpu, 1);

    BIT_TEST(cpu, 0x02);

    print_cpu_expected(15, 1, 1, 2, 0x810c, 0xff);
    print_cpu_state(cpu);

    BIT_TEST(cpu, 0x01);
    print_cpu_expected(15, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 0b01111111);

    BIT_TEST(cpu, 0x00);
    print_cpu_expected(127, 1, 1, 0b01000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    cpu->accumulator = 0xff;

    BIT_TEST(cpu, 0x00);
    print_cpu_expected(0xff, 1, 1, 0b11000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### Branch Tests #####\n\n");

    CLEAR_ALL_FLAGS(cpu);
    LD_immediate_ACC(cpu, 1);
    BRANCH_If_Positive(cpu, 0b10000000);

    print_cpu_expected(1, 1, 1, 0, 0x808c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 0b10000000);
    BRANCH_If_Minus(cpu, 0b01111111);

    print_cpu_expected(0b10000000, 1, 1, 0b10000000, 0x810b, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 0);
    BRANCH_Equal(cpu, 1);

    print_cpu_expected(0, 1, 1, 0b00000010, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 1);
    BRANCH_Not_Equal(cpu, 0b10000000);

    print_cpu_expected(1, 1, 1, 0, 0x808c, 0xff);
    print_cpu_state(cpu);

    BRANCH_Overflow_C(cpu, 0b01111111);

    print_cpu_expected(1, 1, 1, 0, 0x810b, 0xff);
    print_cpu_state(cpu);

    BRANCH_Carry_C(cpu, 1);

    print_cpu_expected(1, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    SET_V_FLAG(cpu);
    SET_C_FLAG(cpu);

    BRANCH_Carry_S(cpu, 0xff);

    print_cpu_expected(1, 1, 1, 0b01000001, 0x810b, 0xff);
    print_cpu_state(cpu);

    BRANCH_Overflow_S(cpu, 1);

    print_cpu_expected(1, 1, 1, 0b01000001, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### Shifts Tests #####\n\n");

    LD_immediate_ACC(cpu, 0b10000001);
    CLEAR_ALL_FLAGS(cpu);

    SHIFT_LEFT_ACC(cpu);

    print_cpu_expected(2, 1, 1, 0b00000001, 0x810c, 0xff);
    print_cpu_state(cpu);

    SHIFT_LEFT_ACC(cpu);

    print_cpu_expected(4, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    for (int i = 0; i < 3; ++i) {
        SHIFT_RIGHT_ACC(cpu);
    }

    print_cpu_expected(0, 1, 1, 0b00000011, 0x810c, 0xff);
    print_cpu_state(cpu);

    CLEAR_Z_FLAG(cpu);
    cpu->mem[10] = 0b10000001;

    SHIFT_LEFT_absolute(cpu, 10);

    print_cpu_expected(0, 1, 1, 0b00000001, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 2 | %i\n\n", cpu->mem[10]);

    SHIFT_LEFT_offset(cpu, 5, 5);

    print_cpu_expected(0, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 4 | %i\n\n", cpu->mem[10]);

    SHIFT_RIGHT_absolute(cpu, 10);

    print_cpu_expected(0, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 2 | %i\n\n", cpu->mem[10]);

    SHIFT_RIGHT_offset(cpu, 5, 5);
    SHIFT_RIGHT_offset(cpu, 8, 2);

    print_cpu_expected(0, 1, 1, 0b00000011, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 0 | %i\n\n", cpu->mem[10]);

    printf("##### Rotations Tests #####\n\n");

    LD_immediate_ACC(cpu, 0b00000110);
    CLEAR_ALL_FLAGS(cpu);

    ROTATION_RIGHT_ACC(cpu);
    ROTATION_RIGHT_ACC(cpu);

    print_cpu_expected(1, 1, 1, 1, 0x810c, 0xff);
    print_cpu_state(cpu);

    ROTATION_RIGHT_ACC(cpu);
    ROTATION_RIGHT_ACC(cpu);

    print_cpu_expected(0b11000000, 1, 1, 0b10000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    ROTATION_LEFT_ACC(cpu);
    ROTATION_LEFT_ACC(cpu);

    CLEAR_N_FLAG(cpu);

    print_cpu_expected(1, 1, 1, 1, 0x810c, 0xff);
    print_cpu_state(cpu);

    ROTATION_LEFT_ACC(cpu);
    ROTATION_LEFT_ACC(cpu);

    print_cpu_expected(0b00000110, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    LD_immediate_ACC(cpu, 1);
    CLEAR_ALL_FLAGS(cpu);

    cpu->mem[10] = 0b00000110;
    ROTATION_RIGHT_absolute(cpu, 10);
    ROTATION_RIGHT_absolute(cpu, 10);

    print_cpu_expected(1, 1, 1, 1, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 1 | %i\n\n", cpu->mem[10]);

    ROTATION_RIGHT_offset(cpu, 5, 5);
    ROTATION_RIGHT_offset(cpu, 2, 8);

    print_cpu_expected(1, 1, 1, 0b10000000, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: %i | %i\n\n", 0b11000000, cpu->mem[10]);

    ROTATION_LEFT_absolute(cpu, 10);
    ROTATION_LEFT_absolute(cpu, 10);

    CLEAR_N_FLAG(cpu);

    print_cpu_expected(1, 1, 1, 1, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 1 | %i\n\n", cpu->mem[10]);

    ROTATION_LEFT_offset(cpu, 1, 9);
    ROTATION_LEFT_offset(cpu, 6, 4);

    print_cpu_expected(1, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("\nExpected: 6 | %i\n\n", cpu->mem[10]);

    printf("##### Jump Tests #####\n\n");

    JUMP_absolute(cpu, 0x1000);

    print_cpu_expected(1, 1, 1, 0, 0x1000, 0xff);
    print_cpu_state(cpu);

    cpu->mem[0x00ff] = 0x0c;
    cpu->mem[0x0100] = 0x81;

    JUMP_indirect(cpu, 0x00ff);

    print_cpu_expected(1, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    printf("##### Break Test #####\n\n");

    cpu->mem[0xfffe] = 0xaa;
    cpu->mem[0xffff] = 0xff;

    stack_test[0xff] = 0x81;
    stack_test[0xfe] = 0x0c;
    stack_test[0xfd] = 0;

    BREAK(cpu);

    print_stack_expected(stack_test, 0xfc);
    print_stack_state(cpu);

    print_cpu_expected(1, 1, 1, 0b00010000, 0xffaa, 0xfc);
    print_cpu_state(cpu);

    printf("##### RTI, RTS, JSR Test #####\n\n");

    RETURN_Interrupt(cpu);

    print_cpu_expected(1, 1, 1, 0, 0x810c, 0xff);
    print_cpu_state(cpu);

    JUMP_Subroutine(cpu, 0xb00b);

    stack_test[0xff] = 0x81;
    stack_test[0xfe] = 0x0e;

    print_stack_expected(stack_test, 0xfd);
    print_stack_state(cpu);

    print_cpu_expected(1, 1, 1, 0, 0xb00b, 0xfd);
    print_cpu_state(cpu);

    RETURN_Subroutine(cpu);

    print_cpu_expected(1, 1, 1, 0, 0x810f, 0xff);
    print_cpu_state(cpu);

    free_cpu(cpu);

    if (my_rom != NULL) {
        free_rom(my_rom);
    }

    return EXIT_SUCCESS;
}
